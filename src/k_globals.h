#ifndef __K_GLOBALS_H___
#define __K_GLOBALS_H___

#include "config.h"
#include "k_taskdescriptor.h"
#include "user_api.h"

typedef struct {
  // Primary Global Fields 
  int task_count;

  // Task Descriptor Pool
  TaskDescriptor tasks[NTASKS];
  TaskDescriptor *task_next, *task_last, *running_task, *event_queues[EVENT_MAX];
  int tid_next;
  unsigned int event_blocked_mask;

  struct {
    TaskDescriptor *head, *tail;
  } taskQueue[NPRIORITIES];
  unsigned int ready_mask;

  // Stack Pool
  KStack stacks[NTASKS]; 
  KStack *stack_next, *stack_last;

  int uart1_cts, uart1_waiting_cts;
} KGlobals;

void KGlobals_init(KGlobals *globals, int kernel_stack_top);

#endif
