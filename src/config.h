#ifndef __CONFIG_H__
#define __CONFIG_H__

#define TID_MAX 0xFFFF
#define NTASKS 128
#define NPRIORITIES 32
#define KERNEL_STACK_SIZE 0x20000
#define TASK_STACK_SIZE 0x10000

#define NAME_SERVER_TID 0x10001

#endif
