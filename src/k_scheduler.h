#ifndef __K_SCHEDULER_H__
#define __K_SCHEDULER_H__

#include "k_globals.h"
#include "k_taskdescriptor.h"

void add_ready_task(KGlobals *globals, TaskDescriptor *td);
TaskDescriptor *get_next_ready_task(KGlobals *globals);
void remove_ready_task(KGlobals *globals, TaskDescriptor *td);

void add_child_task_pointer(KGlobals *globals, TaskDescriptor* parent_td, TaskDescriptor* child_td);
void remove_child_task_pointer(KGlobals *globals, TaskDescriptor* parent_td, TaskDescriptor* child_td);
void reclaim_task_resources(KGlobals *globals, TaskDescriptor* td);
void remove_task_event_queues(KGlobals *globals, TaskDescriptor *td);
void postorder_dfs_task_traversal(
    KGlobals *globals, TaskDescriptor* td, void (*task_action)(KGlobals*, TaskDescriptor*));

#endif
