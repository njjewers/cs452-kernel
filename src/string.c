#include "string.h"

void *memcpy(void *dest, void *src, unsigned int n) {
  unsigned char *dest_bytes = dest, *src_bytes = src;
  while (n > 0) {
    *dest_bytes++ = *src_bytes++;
    --n;
  }
  return dest;
}

void *memset(void *str, int c, unsigned int n) {
  unsigned char *bytes = str;
  while (n-- > 0) *bytes++ = c;
  return str;
}

int strlen(const char *str) {
  int l = 0;
  while (*str++) ++l;
  return l;
}

int strcmp(const char *str1, const char *str2) {
  while (*str1 && *str1 == *str2)
    ++str1, ++str2;
  return *str1 - *str2;
}

int atoi(const char *str) {
  int num = 0;
  while (*str >= '0' && *str <= '9')
    num = num * 10 + *str++ - '0';
  return num;
}

void uitoa(int num, int radix, char *buf) {
  int n = 0, d = 1, digit;
  while (num / d >= radix) d *= radix;
  while (d != 0) {
    digit = num / d;
    num %= d;
    d /= radix;
    if (n || digit > 0 || d == 0) {
      *buf++ = digit + (digit < 10 ? '0' : 'a' - 10);
      ++n;
    }
  }
  *buf = 0;
}

void itoa(int num, int radix, char *buf) {
  if (num < 0) {
    num = -num;
    *buf++ = '-';
  }
  uitoa(num, radix, buf);
}
