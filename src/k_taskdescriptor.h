#ifndef __K_TASKDESCRIPTOR_H__
#define __K_TASKDESCRIPTOR_H__

typedef struct KStack {
  struct KStack *next, *prev;
  int stack_top;
} KStack;

typedef enum {
  TS_READY,
  TS_ZOMBIE,
  TS_BLOCKED_SEND,
  TS_BLOCKED_RECV,
  TS_BLOCKED_REPLY,
  TS_BLOCKED_EVENT,
  TS_MAX
} TaskState;

typedef struct TaskDescriptor {
  int tid;
  int priority;
  TaskState state;
  int psr;
  int *sp;
  KStack *stack;
  struct TaskDescriptor *next, *prev, 
                        *next_ready, *prev_ready, 
                        *parent, *child, *next_sibling,
                        *waiting_senders, *next_waiting_sender,
                        *next_event_waiting;
  struct {
    char *msg, *reply;
    int msglen, rplen, *sender_tid;
    struct TaskDescriptor *recipient;
  } message;
  char output_byte;
  int running_time; 

  void (*at_exit)(void);
} TaskDescriptor;

static inline int tid_task_id(unsigned int tid) {
  return tid & 0x0000FFFF;
}

static inline int tid_td_index(unsigned int tid) {
  return (tid == 0) ? 0 : (tid & 0xFFFF0000) >> 16;
}

#endif
