#ifndef __USER_SYSTEM_UART_H__
#define __USER_SYSTEM_UART_H__

#define UARTSERVICE_NAME "UartService"

typedef enum {
  UARTSERVER_GETC,
  UARTSERVER_PUTC,
  UARTSERVER_PUTSTR
} UartServiceRequestType;

typedef struct {
  UartServiceRequestType type;
  int uart;
  union {
    char c;
    char *s;
  };
} UartServiceRequest;

void system_uart_server();

#endif
