#ifndef __UTIL_RINGBUFFER_H__
#define __UTIL_RINGBUFFER_H__

typedef struct {
  char *buffer;
  int size;
  int readIndex;
  int writeIndex;
} RingBuffer;

void RingBuffer_init(RingBuffer *buf, char* buffer, int size);

int RingBuffer_hasElements(RingBuffer *buf);

void RingBuffer_push(RingBuffer *buf, char c);
void RingBuffer_pushstr(RingBuffer *buf, char* s);

char RingBuffer_pop(RingBuffer *buf);

char RingBuffer_peek(RingBuffer *buf);

#endif
