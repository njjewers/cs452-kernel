#include "user_system_tasks.h"
#include "user_api.h"
#include "util_tid_heap.h"
#include "string.h"

void system_name_server() {
  int sender_tid, not_found = -1, i;
  NameServiceRequest ns_request;

  struct {
    char* service_name;
    int service_tid;
  } services[] = {
    {IDLETASK_NAME, -1},
    {CLOCKSERVICE_NAME, -1},
    {UARTSERVICE_NAME, -1},
    {TRAINS_UI_SERVICE, -1},
    {TRAINS_CONTROL_SERVICE, -1},
    {TRAINS_USER_INPUT_SERVICE, -1}
  };

  for (;;) {
    Receive(&sender_tid, (char*) &ns_request, sizeof(ns_request));

    for(i = 0; i < ARRAY_SIZE(services); i++)
      if(strcmp(services[i].service_name, ns_request.name) == 0)
        break;

    if (i == ARRAY_SIZE(services)) {
      Reply(sender_tid, (char *) &not_found, sizeof(not_found));
    } else if(ns_request.type == NAMESERVER_REGISTER) {
      services[i].service_tid = sender_tid;
      Reply(sender_tid, 0, 0);
    } else if(ns_request.type == NAMESERVER_WHOIS) {
      Reply(sender_tid, (char *) &services[i].service_tid, sizeof(services[i].service_tid));
    }
  }
}

static void system_clock_notifier() {
  int clock_service_tid = MyParentTid();
  ClockServiceRequest tickRequest;
  tickRequest.type = CLOCKSERVER_TICK;

  for (;;) {
    AwaitEvent(EVENT_TIMER_TICK, 0);
    Send(clock_service_tid, (char*)&tickRequest, sizeof(tickRequest), (char*)0, 0);
  }
}

void system_clock_server() {
  int tid, tickCount = 0;
  ClockServiceRequest request;
  TidHeap delayedTasks;
  TidHeap_init(&delayedTasks);

  RegisterAs(CLOCKSERVICE_NAME);
  Create(31, system_clock_notifier);

  for (;;) {
    Receive(&tid, (char*)&request, sizeof(request));

    switch (request.type) {
      case CLOCKSERVER_TICK:
        Reply(tid, (char *)0, 0);
        tickCount++;
        while (TidHeap_peekPriority(&delayedTasks) < tickCount) {
          tid = TidHeap_popTid(&delayedTasks);
          Reply(tid, (char*)0, 0);
        }
        break;
      case CLOCKSERVER_QUERY:
        Reply(tid, (char*)&tickCount, sizeof(tickCount));
        break;
      case CLOCKSERVER_DELAY:
        TidHeap_push(&delayedTasks, tickCount + request.arg, tid);
        break;
      case CLOCKSERVER_DELAYUNTIL:
        TidHeap_push(&delayedTasks, request.arg, tid);
        break;
    }
  }
}

void system_idle_task() {
  RegisterAs(IDLETASK_NAME);
  while (1);
}
