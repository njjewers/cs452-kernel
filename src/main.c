#include "k_globals.h"
#include "k_task.h"
#include "k_scheduler.h"
#include "user_tasks.h"
#include "ts7200.h"
#include "k_interrupts.h"

int main() {
  set_caches(1);
  setspeed(COM2, 115200);
  set_lcrh(COM2, 0, 1);
  setspeed(COM1, 2400);
  set_lcrh(COM1, 0, 2);

  set_timer_interrupt(2, 1);
  start_timer(2, CLK_2KHZ, 19);
  start_system_timer();

  volatile int *vic1_enable = (int *)(VIC1_BASE + VIC_INT_ENABLE_OFFSET),
           *vic2_enable = (int *)(VIC2_BASE + VIC_INT_ENABLE_OFFSET),
           *uart1_ctrl = (int *)(UART1_BASE + UART_CTLR_OFFSET),
           *uart2_ctrl = (int *)(UART2_BASE + UART_CTLR_OFFSET);
  *vic1_enable = UART2_RXIRQ_MASK;
  *vic2_enable = UART1_IRQ_MASK;
  *uart1_ctrl |= MSIEN_MASK | RIEN_MASK;
  *uart2_ctrl |= TIEN_MASK | RIEN_MASK;

  register int kernel_stack_top asm ("fp");
  KGlobals globals;
  KGlobals_init(&globals, kernel_stack_top);

  k_create_task(&globals, 1, first_user_task);
  while (globals.task_count > 0) {
    globals.running_task = get_next_ready_task(&globals);
    if (globals.running_task == 0) break; 

    k_run_task(&globals);
  }

  disable_all_interrupts();
  return 0;
}
