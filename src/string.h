#ifndef __STRING_H__
#define __STRING_H__

void *memcpy(void *dest, void *src, unsigned int n);
void *memset(void *str, int c, unsigned int n);
int strlen(const char *str);
int strcmp(const char *str1, const char *str2);
int atoi(const char *str);
void uitoa(int num, int radix, char *buf);
void itoa(int num, int radix, char *buf);

#endif
