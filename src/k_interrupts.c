#include "k_interrupts.h"
#include "k_scheduler.h"
#include "k_globals.h"
#include "k_task.h"
#include "ts7200.h"

void set_timer_interrupt(int timer, int enable) {
  volatile int *vic_enable = 0, *vic_disable = 0; 
  int timer_mask = 0;
  switch (timer) {
    case 1:
      vic_enable = (int *)(VIC1_BASE + VIC_INT_ENABLE_OFFSET);
      vic_disable = (int *)(VIC1_BASE + VIC_INT_DISABLE_OFFSET);
      timer_mask = TIMER1_IRQ_MASK;
      break;
    case 2:
      vic_enable = (int *)(VIC1_BASE + VIC_INT_ENABLE_OFFSET);
      vic_disable = (int *)(VIC1_BASE + VIC_INT_DISABLE_OFFSET);
      timer_mask = TIMER2_IRQ_MASK;
      break;
    case 3:
      vic_enable = (int *)(VIC2_BASE + VIC_INT_ENABLE_OFFSET);
      vic_disable = (int *)(VIC2_BASE + VIC_INT_DISABLE_OFFSET);
      timer_mask = TIMER3_IRQ_MASK;
      break;
  }
  if (enable) *vic_enable = timer_mask;
  else *vic_disable = timer_mask;
}

// writing any value to timer_clear register clears the interrupt in VICx_BASE
void clear_timer_interrupt(int timer) {
  volatile int *timer_clear = 0;
  switch (timer) {
    case 1:
      timer_clear = (int *)(TIMER1_BASE + CLR_OFFSET);
      break;
    case 2:
      timer_clear = (int *)(TIMER2_BASE + CLR_OFFSET);
      break;
    case 3:
      timer_clear = (int *)(TIMER3_BASE + CLR_OFFSET);
      break;
  }
  *timer_clear = 1;
}

void disable_all_interrupts() {
  volatile int *irq_disable1, *irq_disable2;
  irq_disable1 = (int *)(VIC1_BASE + VIC_INT_DISABLE_OFFSET);
  irq_disable2 = (int *)(VIC2_BASE + VIC_INT_DISABLE_OFFSET);

  *irq_disable1 = 0xffffffff;
  *irq_disable2 = 0xffffffff;
}

static void clear_event_queue(KGlobals *globals, int queue, int ret) {
  while (globals->event_queues[queue]) {
    TaskDescriptor *waiting = globals->event_queues[queue];
    globals->event_queues[queue] = waiting->next_event_waiting;
    waiting->next_event_waiting = 0;

    k_task_set_return_int(waiting, ret);
    waiting->state = TS_READY;
    add_ready_task(globals, waiting);
  }
  globals->event_blocked_mask &= ~(1 << queue);
}

static int pop_uart_transmit_queue(KGlobals *globals, int chan) {
  volatile int *data = 0;
  int queue = 0, non_empty = 1;
  TaskDescriptor *waiting = 0;
  switch (chan) {
    case COM1:
      data = (int *)(UART1_BASE + UART_DATA_OFFSET);
      queue = EVENT_UART1_TRANSMIT;
      break;
    case COM2:
      data = (int *)(UART2_BASE + UART_DATA_OFFSET);
      queue = EVENT_UART2_TRANSMIT;
      break;
  } 
  waiting = globals->event_queues[queue];
  *data = waiting->output_byte;

  globals->event_queues[queue] = waiting->next_event_waiting;
  waiting->next_event_waiting = 0;
  if (!globals->event_queues[queue]) {
    globals->event_blocked_mask &= ~(1 << queue);
    non_empty = 0;
  }

  k_task_set_return_int(waiting, 0);
  waiting->state = TS_READY;
  add_ready_task(globals, waiting);
  return non_empty;
}

void handle_irq(KGlobals *globals) {
  int vic1 = *(int *)VIC1_BASE, vic2 = *(int *)VIC2_BASE;
  if (vic1 & TIMER1_IRQ_MASK) {
    clear_timer_interrupt(1);
  }

  if (vic1 & TIMER2_IRQ_MASK) {
    clear_timer_interrupt(2);
    clear_event_queue(globals, EVENT_TIMER_TICK, 0);
  }

  if (vic2 & TIMER3_IRQ_MASK) {
    clear_timer_interrupt(3);
  }

  if (vic1 & UART2_TXIRQ_MASK) {
    if (!pop_uart_transmit_queue(globals, COM2)) {
      volatile int *vic1_disable = (int *)(VIC1_BASE + VIC_INT_DISABLE_OFFSET);
      *vic1_disable = UART2_TXIRQ_MASK;
    }
  }

  if (vic1 & UART2_RXIRQ_MASK) {
    char byte = *(int *)(UART2_BASE + UART_DATA_OFFSET);
    clear_event_queue(globals, EVENT_UART2_RECEIVE, byte);
  }

  if (vic2 & UART1_IRQ_MASK) {
    volatile int *uart1_intr = (int *)(UART1_BASE + UART_INTR_OFFSET),
             *uart1_ctrl = (int *)(UART1_BASE + UART_CTLR_OFFSET);
    int intr_mask = *uart1_intr;

    if (intr_mask & MSI_MASK) {
      *uart1_intr = 0;
      if (*(int *)(UART1_BASE + UART_FLAG_OFFSET) & CTS_MASK) {
        globals->uart1_cts = 1;
        if (globals->uart1_waiting_cts) {
          *uart1_ctrl |= TIEN_MASK;
          globals->uart1_waiting_cts = 0;
        } 
      }
    }
   
    if (intr_mask & TIS_MASK) {
      int disable_tis = 1;
      if (globals->uart1_cts) {
        if (pop_uart_transmit_queue(globals, COM1)) {
          disable_tis = 0;
        }
        globals->uart1_cts = 0;
      } else {
        globals->uart1_waiting_cts = 1;
      }
      if (disable_tis) {
        *uart1_ctrl &= ~TIEN_MASK;
      }
    }

    if (intr_mask & RIS_MASK) {
      char byte = *(int *)(UART1_BASE + UART_DATA_OFFSET);
      clear_event_queue(globals, EVENT_UART1_RECEIVE, byte);
    }
  }
}
