#include "ts7200.h"

void set_caches(int enable) {
  register unsigned int mask;
  asm volatile ("MRC P15, 0, %[mask], C1, C0, 0" : [mask] "=r" (mask));
  if (enable) mask |= ICACHE_MASK | DCACHE_MASK;
  else mask &= ~(ICACHE_MASK | DCACHE_MASK);
  asm volatile ("MCR P15, 0, %[mask], C1, C0, 0" : : [mask] "r" (mask));
}

void start_timer(int timer, int clock, int initial) {
  volatile int *timer_ctrl = 0, *timer_load = 0;
  switch (timer) {
    case 1:
      timer_ctrl = (int *)(TIMER1_BASE + CRTL_OFFSET);
      timer_load = (int *)(TIMER1_BASE + LDR_OFFSET);
      break;
    case 2:
      timer_ctrl = (int *)(TIMER2_BASE + CRTL_OFFSET);
      timer_load = (int *)(TIMER2_BASE + LDR_OFFSET);
      break;
    case 3:
      timer_ctrl = (int *)(TIMER3_BASE + CRTL_OFFSET);
      timer_load = (int *)(TIMER3_BASE + LDR_OFFSET);
      break;
  }
  *timer_ctrl &= ~ENABLE_MASK;
  *timer_load = initial;
  switch (clock) {
    case CLK_2KHZ:
      *timer_ctrl &= ~CLKSEL_MASK;
      break;
    case CLK_508KHZ:
      *timer_ctrl |= CLKSEL_MASK;
      break;
  }
  *timer_ctrl |= ENABLE_MASK | MODE_MASK;
}

void start_system_timer() {
  volatile int *timer_ctrl = (int *)TIMER4_HIGH;
  *timer_ctrl &= ~TIMER4_ENABLE_MASK;
  *timer_ctrl |= TIMER4_ENABLE_MASK;
}

void set_lcrh(int chan, int fifo_enable, int stop_bits) {
  volatile int *line = 0;
  int mask;
  switch(chan) {
    case COM1:
      line = (int *)(UART1_BASE + UART_LCRH_OFFSET);
      break;
    case COM2:
      line = (int *)(UART2_BASE + UART_LCRH_OFFSET);
      break;
  }
  mask = *line;
  if (fifo_enable) mask |= FEN_MASK;
  else mask &= ~FEN_MASK;
  if (stop_bits == 1) mask &= ~STP2_MASK;
  else if (stop_bits == 2) mask |= STP2_MASK;
  *line = mask;
}

void setspeed(int chan, int speed) {
  volatile int *high = 0, *low = 0;
  switch(chan) {
    case COM1:
      high = (int *)(UART1_BASE + UART_LCRM_OFFSET);
      low = (int *)(UART1_BASE + UART_LCRL_OFFSET);
      break;
    case COM2:
      high = (int *)(UART2_BASE + UART_LCRM_OFFSET);
      low = (int *)(UART2_BASE + UART_LCRL_OFFSET);
      break;
  }
  switch(speed) {
    case 115200:
      *high = 0x0;
      *low = 0x3;
      break;
    case 2400:
      *high = 0x0;
      *low = 0xBF;
      break;
  }
}
