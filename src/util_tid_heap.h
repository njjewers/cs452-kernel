#ifndef __UTIL_PID_HEAP_H__
#define __UTIL_PID_HEAP_H__

#include "config.h"

typedef struct {
  int priority;
  int tid;
} TidHeapEntry;

typedef struct {
  int count;
  TidHeapEntry entries[NTASKS];
} TidHeap;

void TidHeap_init(TidHeap * heap);
void TidHeap_push(TidHeap * heap, int priority, int tid);
int TidHeap_peekPriority(TidHeap* heap);
int TidHeap_popTid(TidHeap* heap);

#endif
