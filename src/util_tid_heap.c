#include "util_tid_heap.h"

#define INT_MAX 0x7fffffff

void TidHeap_init(TidHeap * heap) {
  heap->count = 0;
}

void TidHeap_push(TidHeap * heap, int priority, int tid) {
  int i = (heap->count)++;

  while (i>0) {
    int parent = (i-1) >> 1;
    if (heap->entries[parent].priority > priority) {
      // This would invalidate heap property, swap w/parent
      heap->entries[i] = heap->entries[parent];
    } else {
      break;
    }
    i = parent;
  }
  heap->entries[i].priority = priority;
  heap->entries[i].tid = tid;
}

int TidHeap_peekPriority(TidHeap* heap) {
  return heap->count == 0 ? INT_MAX : heap->entries[0].priority;
}

int TidHeap_popTid(TidHeap* heap) {
  if (heap->count == 0) return -1;
  int ret = heap->entries[0].tid;

  int tail = --(heap->count);
  if (tail == 0) {
    return ret; // Heap now empty, fast return
  }

  int priority = heap->entries[tail].priority;

  int i = 0;
  int child = 1;
  while (child < heap->count) {
    if ((child+1 < heap->count) && (heap->entries[child+1].priority < heap->entries[child].priority)) {
      child++;
    }
    if (heap->entries[child].priority < priority) {
      // Would invalidate heap property
      heap->entries[i] = heap->entries[child];
    } else {
      // Would not invalidate heap property, break
      break;
    }
    i = child;
    child = (i<<1) + 1;
  }

  heap->entries[i].priority = priority;
  heap->entries[i].tid = heap->entries[tail].tid;

  return ret;
}
