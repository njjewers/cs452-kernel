#include <util_ringbuffer.h>

static void RingBuffer_advanceIndex(int *index, int *size);

void RingBuffer_init(RingBuffer *buf, char* buffer, int size) {
  buf->buffer = buffer;
  buf->size = size;
  buf->readIndex = 0;
  buf->writeIndex = 0;
}

int RingBuffer_hasElements(RingBuffer *buf) {
  return (buf->writeIndex != buf->readIndex) ? 1 : 0;
}

void RingBuffer_push(RingBuffer *buf, char c) {
  buf->buffer[buf->writeIndex] = c;
  RingBuffer_advanceIndex(&(buf->writeIndex), &(buf->size));
}

void RingBuffer_pushstr(RingBuffer *buf, char *s) {
  while (*s) {
    buf->buffer[buf->writeIndex] = *s;
    RingBuffer_advanceIndex(&(buf->writeIndex), &(buf->size));
    s++;
  }
}

char RingBuffer_pop(RingBuffer *buf) {
  char c = buf->buffer[buf->readIndex];
  RingBuffer_advanceIndex(&(buf->readIndex), &(buf->size));
  return c;
}

char RingBuffer_peek(RingBuffer *buf) {
  return buf->buffer[buf->readIndex];
}

static void RingBuffer_advanceIndex(int *index, int *size) {
  *index = (*index == *size - 1) ? 0 : (*index) + 1;
}

