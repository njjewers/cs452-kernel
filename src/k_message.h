#ifndef __K_MESSAGE_H__
#define __K_MESSAGE_H__

#include "k_taskdescriptor.h"
#include "k_globals.h"
#include "k_scheduler.h"
#include "k_task.h"

void k_send_message(KGlobals *globals, int tid, char *msg, int msglen, char *reply, int rplen);
void k_receive_message(KGlobals *globals, int *tid, char *msg, int msglen);
void k_reply_message(KGlobals *globals, int tid, char *reply, int rplen);
void remove_task_send_queue(KGlobals *globals, TaskDescriptor *td);
void clear_reply_blocked(KGlobals *globals, TaskDescriptor *td);

#endif
