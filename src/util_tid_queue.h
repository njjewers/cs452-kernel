#ifndef __UTIL_PID_QUEUE_H__
#define __UTIL_PID_QUEUE_H__

#include "config.h"

typedef struct {
  int head;
  int tail;
  int elements[NTASKS];
} TidQueue;

void TidQueue_init(TidQueue* queue);
int TidQueue_hasElements(TidQueue* queue);
void TidQueue_push(TidQueue* queue, int tid);
int TidQueue_pop(TidQueue* queue);

#endif

