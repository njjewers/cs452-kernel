#include "train.h"
#include "user_api.h"
#include "train_control_server.h"
#include "trains_ui.h"
#include "ts7200.h"

void stop_by_distance(int uart_tid, int clock_tid, Train* train, int distance_till_full_stop, track_node* track);
void flip_next_three_switches_on_path(int uart_tid, int switches[], Train* train, track_node* node);
int train_get_stopping_distance(Train* train);
void train_reverse_direction(int uart_tid, Train* train, int delay); // delay in 10 ms ticks
void navigate_to_random_exit(int uart_tid, int clock_tid, Train* train, int delay, track_node* current_exit_node, track_node* track);
void process_collision_avoidance(int uart_tid, Train *train, Train *other_trains, track_node* track);

Train* get_train(Train* trains, int train_id) {
  Train* train = 0; int i;
  for(i=0; i<TOTAL_TRAINS; i++) {
    if(trains[i].id == train_id) {
      train = &trains[i];
      break;
    }
  }
  return train;
}

void set_train_velocity(Train* train, int speed) {
  if(!train) return;
  if(speed < 0 || speed > SPEED_MAX) speed = 0;

  train->velocity = get_velocity_for_speed(train, speed);
  train->prev_speed = train->speed;
  train->speed = speed;
}

float get_velocity_for_speed(Train* train, int speed) {
  if(!train) return -1;
  if(speed < 0 || speed > SPEED_MAX) speed = 0;

  float velocity = 0;
  float timing = (float) train->timings[speed] * TIMER4_SEC_RATIO; // in sec
  if(timing) velocity = C13_E7_TRACK_B_DISTANCE / timing;
  return velocity;
}

int get_time_for_distance(Train* train, int distance) { //time in ms, distance in mm
  if(!train) return 0;
  if(train->velocity <= 0 || distance <= 0) return 0;
  return (int) ((float) distance / train->velocity * 1000);
}

void train_adjust_velocity(int uart_tid, int clock_tid, Train* train, track_node* track, int switches[]) {
  if(!train) return;

  train->estimated_time_prev_sensors_edge = train->estimated_time_current_sensors_edge;
  train->estimated_time_current_sensors_edge = get_time_for_distance(train, train->current_sensors_edge.dist);

  train->actual_time_prev_sensor = train->actual_time_current_sensor;
  train->actual_time_current_sensor = read_system_timer() * TIMER4_MS_RATIO; //in ms
  train->actual_time_prev_sensors_edge = train->actual_time_current_sensor - train->actual_time_prev_sensor;
  train->prev_sensors_edge_time_estimate_error = train->actual_time_prev_sensors_edge - train->estimated_time_prev_sensors_edge;

  // if (abs(train->prev_sensors_edge_time_estimate_error) < 200) {
  //   float prev_velocity = 1000 * (float)train->prev_sensors_edge.dist / (float)train->actual_time_prev_sensors_edge;
  //   train->velocity = 0.125f * train->velocity + 0.875f * prev_velocity;
  // }
}

void reverse_current_sensors_edge(Train* train) {
  if(train->current_sensors_edge.src && train->current_sensors_edge.dest) {
    track_node* src = train->current_sensors_edge.dest->reverse;
    train->current_sensors_edge.dest = train->current_sensors_edge.src->reverse;
    train->current_sensors_edge.src = src;
  }
}

TrackPath* find_track_node_on_path(Train* train, track_node* node) {
  if(!node) return 0;
  int i = 0;
  TrackPath* trackpath_node=0;
  for(i=0;i<TRACK_MAX;i++) {
    if(&(train->path[i]) == train->pathstart) break;
  }
  while(1) {
    if(i>=TRACK_MAX) break;
    if(train->path[i].node == node) {
      trackpath_node = &(train->path[i]);
      break;
    }
    i++;
  }
  return trackpath_node;
}

int find_track_node_index_on_path(Train* train, track_node* node) {
  if(!node) return 0;
  int i = 0, track_node_index = -1;
  for(i=0;i<TRACK_MAX;i++) {
    if(&(train->path[i]) == train->pathstart) break;
  }
  while(1) {
    if(i>=TRACK_MAX) break;
    if(train->path[i].node == node) {
      track_node_index = i;
      break;
    }
    i++;
  }
  return track_node_index;
}

void train_erase_path(Train* train) {
  train->pathstart = 0;
  train->destination_node = 0;
  train->prev_sensor_on_path = 0;
  train->current_sensor_on_path = 0;
  train->next_sensor_on_path = 0;
  train->second_next_sensor_on_path = 0;
}

int is_first_sensor_on_path(Train* train) {
  return (train->prev_sensor_on_path == 0) ? 1 : 0;
}

void print_train_is_off_path(int uart_tid, Train* train) {
  char* no_node_name = "0";
  TrackPath *expected_current_sensor = 0;

  if(!is_first_sensor_on_path(train)) {
    expected_current_sensor = find_track_node_on_path(train, train->prev_sensor_on_path->next_sensor);
  }
  Printf(uart_tid, 2, "\033[%d;%dHGot off-track! Expected: %s, prev %s, current %s, next %s.", 2, 3,
    (expected_current_sensor ? expected_current_sensor->node->name : no_node_name),
    (train->prev_sensor_on_path ? train->prev_sensor_on_path->node->name : no_node_name),
    (train->current_sensor_on_path ? train->current_sensor_on_path->node->name : no_node_name),
    (train->next_sensor_on_path ? train->next_sensor_on_path->node->name : no_node_name));
}

static void train_assign_new_current_sensor_on_path(int uart_tid, Train* train, track_node* current_sensor, int time_now) { //time in ms
  char* no_node_name = "0";
  int next_sensor_time_delta = 0, second_next_sensor_time_delta = 0; // in ms ticks
  train->prev_sensor_on_path = train->current_sensor_on_path;
  train->current_sensor_on_path = find_track_node_on_path(train, current_sensor);
  if(train->current_sensor_on_path) {
    train->next_sensor_on_path = find_track_node_on_path(train, train->current_sensor_on_path->next_sensor);
    train->next_sensor_on_path_distance = 0;
    train->next_sensor_on_path_eta = 0;
    if(train->next_sensor_on_path) {
      train->next_sensor_on_path_distance = train->current_sensor_on_path->distance_to_next_sensor;
      next_sensor_time_delta = get_time_for_distance(train, train->next_sensor_on_path_distance);
      train->next_sensor_on_path_eta = time_now + next_sensor_time_delta;
      train->second_next_sensor_on_path = find_track_node_on_path(train, train->next_sensor_on_path->next_sensor);

      TrainControlRequest request = {TRAINCONTROL_SENSOR_TIMEOUT, train->id,
          train->next_sensor_on_path->node->num,
          (int) (next_sensor_time_delta + TRAIN_SENSOR_ATTRIB_TIMEOUT_MS+300)/10}; //800ms timeout
      send_delayed_track_command(&request);

      if(train->second_next_sensor_on_path) {
        train->second_next_sensor_on_path_distance = train->next_sensor_on_path_distance + train->next_sensor_on_path->distance_to_next_sensor;
        second_next_sensor_time_delta = get_time_for_distance(train, train->second_next_sensor_on_path_distance);
        train->second_next_sensor_on_path_eta = time_now + second_next_sensor_time_delta;

        TrainControlRequest request = {TRAINCONTROL_SENSOR_TIMEOUT, train->id,
            train->second_next_sensor_on_path->node->num,
            (int) (second_next_sensor_time_delta + TRAIN_SENSOR_ATTRIB_TIMEOUT_MS+300)/10}; //800ms timeout 
        send_delayed_track_command(&request);
      } else {
        train->second_next_sensor_on_path_distance = 0;
        train->second_next_sensor_on_path_eta = 0;
      }
    }
  }

  Printfrect(uart_tid, 2, 12, get_train_stats_column(train), STATS_COLUMN_WIDTH, 0,
    "Prev sensor: %s\n"
    "Current sensor: %s\n"
    "Next sensor: %s, dist: %d mm\n"
    "Next sensor ETA: %t\n"
    "2 next sen: %s, dist: %d mm\n"
    "2 next sen ETA: %t\n",
    (train->prev_sensor_on_path ? train->prev_sensor_on_path->node->name : no_node_name),
    (train->current_sensor_on_path ? train->current_sensor_on_path->node->name : no_node_name),
    (train->next_sensor_on_path ? train->next_sensor_on_path->node->name : no_node_name),
    train->next_sensor_on_path_distance, (int) train->next_sensor_on_path_eta/10,
    (train->second_next_sensor_on_path ? train->second_next_sensor_on_path->node->name : no_node_name),
    train->second_next_sensor_on_path_distance, (int) train->second_next_sensor_on_path_eta/10);
}

void correct_path_when_off_track(int uart_tid, Train* train, track_node* track) {
  TrainControlRequest request = {TRAINCONTROL_TR, train->id, 0, 0};
  send_delayed_track_command(&request);

  if(train->destination_node){
    Printf(uart_tid, 2, "\033[%d;%dHRecalculating path to %s", 2, 65, train->destination_node->name);
    int destination_node_id = find_track_node_id_by_name(track, train->destination_node->name);
    TrainControlRequest request2 = {TRAINCONTROL_MV, train->id, train->speed, 300, destination_node_id};  //slow down for 3 seconds
    send_delayed_track_command(&request2);
  }
}

void train_adjust_path(int uart_tid, int clock_tid, Train* train, track_node* current_node, track_node* track, int switches[],
    Train *other_trains) {
  if(!train->pathstart || !current_node) return;

  if(train->destination_node) {
    Printfrect(uart_tid, 2, 18, get_train_stats_column(train), STATS_COLUMN_WIDTH, 0,
        "Train %d dest: %s", train->id, train->destination_node->name);
  }

  //sending speed=0, but should've been slowing down till now
  if(current_node == train->destination_node) {
    Printf(uart_tid, 2, "\033[%d;%dHEmergency stop", 2, 3);
    TrainControlRequest request = {TRAINCONTROL_TR, train->id, 0, 0};
    train_erase_path(train);
    send_delayed_track_command(&request);
    return;
  }

  int time_now = Time(clock_tid) * 10; //in ms
  if(current_node && current_node->type == NODE_SENSOR) {
    train_assign_new_current_sensor_on_path(uart_tid, train, current_node, time_now);
  }

  flip_next_three_switches_on_path(uart_tid, switches, train, current_node);

  int stop_dist = train_get_stopping_distance(train);
  if(!train->next_sensor_on_path && !train->stop_command_sent) {
    Printf(uart_tid, 2, "\033[%d;%dHStopping from last sensor", 2, 3);
    stop_by_distance(uart_tid, clock_tid, train, train->current_sensor_on_path->distance_to_end, track);
  } else {
    if(train->next_sensor_on_path->distance_to_end < stop_dist && !train->stop_command_sent) {
      Printf(uart_tid, 2, "\033[%d;%dHStopping from pre-last sensor %s", 2, 3, current_node->name);
      stop_by_distance(uart_tid, clock_tid, train, train->current_sensor_on_path->distance_to_end, track);
    }
  }

  process_collision_avoidance(uart_tid, train, other_trains, track);
}

// issues a speed=0 based on the train stopping distance
// if stopping_distance is smaller than distance_till_full_stop, speed=0 will be delayed appropriately
void stop_by_distance(int uart_tid, int clock_tid, Train* train, int distance_till_full_stop, track_node* track) { //in mm
  if(train->stop_command_sent) return;
  int delay_till_speed0 = 0; // in 10ms ticks
  int distance_till_speed0 = distance_till_full_stop - train_get_stopping_distance(train);
  if(train->destination_node && train->destination_node->type == NODE_EXIT) {
    distance_till_speed0 += 150; // if exit node, add 15cm for safety
  }

  delay_till_speed0 = (distance_till_speed0 / (train->velocity/1000)) / 10;
  TrainControlRequest request = {TRAINCONTROL_TR, train->id, 0, delay_till_speed0};
  send_delayed_track_command(&request);

  //TODO: TEMPORARY, REFACTOR INTO DEMO SCENARIO TASKS
  if(train->demo_id == 1 && train->destination_node && train->destination_node->type == NODE_EXIT) {
    int reverse_speed_delay = delay_till_speed0 + 300; //TODO: stopping time is hardcoded to 3 seconds
    train_reverse_direction(uart_tid, train, reverse_speed_delay);
    navigate_to_random_exit(uart_tid, clock_tid, train, reverse_speed_delay + 10, train->destination_node, track);
  }
  if(train->demo_id == 2 && train->destination_node && train->destination_node->num == 21) { //B6
    int track_node_id = find_track_node_id_by_name(track, "D14");
    TrainControlRequest request2 = {TRAINCONTROL_MV, train->id, 10, 300, track_node_id}; // in 3 sec
    send_delayed_track_command(&request2);
  }
  if(train->demo_id == 2 && train->destination_node && train->destination_node->num == 61) { //D14
    int track_node_id = find_track_node_id_by_name(track, "B6");
    TrainControlRequest request2 = {TRAINCONTROL_MV, train->id, 10, 300, track_node_id}; // in 3 sec
    send_delayed_track_command(&request2);
  }

  train_erase_path(train);
  train->stop_command_sent = 1;
  Printf(uart_tid, 2, "\033[%d;%dHStopping delay %d ms, dist_till_speed0 %d mm", 2, 3, delay_till_speed0, distance_till_speed0);
}

void flip_all_switches_for_path(int uart_tid, Train* train, int switches[]) {
  int i=0;
  for(i=0;i<TRACK_MAX;i++) {
    if(&(train->path[i]) == train->pathstart) break;
  }
  while(1) {
    if(i>=TRACK_MAX) break;
    if(train->path[i].node == train->destination_node) break;
    if(train->path[i].node->type == NODE_BRANCH) {
      flip_switch_on_path(uart_tid, switches, train, train->path[i].node, 0);
    }
    i++;
  }
}

void flip_next_three_switches_on_path(int uart_tid, int switches[], Train* train, track_node* node) {
  char* no_node_name = "0";
  TrackPath* path_node=0;
  int i = 0, distance_till_branch=0, time_till_branch=0, time_till_switch_flip=0;

  if(!node) return;
  while(1) {
    if(i>2) break;
    path_node = find_track_node_on_path(train, node);
    if(!path_node || path_node->distance_to_next_branch == -1) break;
    distance_till_branch += path_node->distance_to_next_branch;
    time_till_branch = distance_till_branch / (train->velocity/1000);

    Printfrect(uart_tid, 2, 19+i, get_train_stats_column(train), STATS_COLUMN_WIDTH, 0,
        "Next branch: %s, %d ms", (path_node->next_branch ? path_node->next_branch->name : no_node_name), time_till_branch);

    time_till_switch_flip = (time_till_branch-1000>0) ? time_till_branch-1000 : 0;
    flip_switch_on_path(uart_tid, switches, train, path_node->next_branch, time_till_switch_flip);

    node = path_node->next_branch;
    i++;
  }
}

void flip_switch_on_path(int uart_tid, int switches[], Train* train, track_node* node, int delay) { //delay in ms
  TrackPath *branch_node_on_path=0;
  int direction=0, switch_position_to_set=0, switch_position_current=0, switch_id=0;
  if(!node) return;
  branch_node_on_path = find_track_node_on_path(train, node);

  if(branch_node_on_path && branch_node_on_path->node->type == NODE_BRANCH) {
    direction = branch_node_on_path->direction;
    switch_position_to_set = (direction == DIR_STRAIGHT) ? SWITCH_STRAIGHT : SWITCH_CURVED;
    switch_id = branch_node_on_path->node->num;
    switch_position_current = switches[switch_id_to_index(switch_id)];

    if(switch_position_to_set != switch_position_current) {
      if(is_a_center_switch(switch_id)) delay = 0;
      Printf(uart_tid, 2, "\033[%d;%dHFlipping switch %d to %d, delay %d ms", 2, 3, switch_id, switch_position_to_set, delay);
      TrainControlRequest request = {TRAINCONTROL_SW, switch_id, switch_position_to_set, (int) delay/10};
      send_delayed_track_command(&request);
    }

    if(is_a_center_switch(switch_id)) {
      switch_position_to_set = (switch_position_to_set == SWITCH_STRAIGHT) ? SWITCH_CURVED : SWITCH_STRAIGHT;
      switch_id = get_opposite_center_switch(switch_id);
      switch_position_current = switches[switch_id_to_index(switch_id)];

      if(switch_position_to_set != switch_position_current) {
        Printf(uart_tid, 2, "\033[%d;%dHFlipping switch %d to %d, delay %d ms", 2, 3, switch_id, switch_position_to_set, delay);
        TrainControlRequest request2 = {TRAINCONTROL_SW, switch_id, switch_position_to_set, (int) delay/10};
        send_delayed_track_command(&request2);
      }
    }
  }
}

int train_get_stopping_distance(Train* train){
  int train_body_size = 0;
  if(train->movement_direction == DIRECTION_FORWARD) {
    train_body_size = TRAIN_LENGTH_TILL_SENSOR_HOOK;
  } else {
    train_body_size = TRAIN_LENGTH - TRAIN_LENGTH_TILL_SENSOR_HOOK;
  }
  return train->stopping_distance[train->speed] + train_body_size;
}

void train_reverse_direction(int uart_tid, Train* train, int delay){
  if(train->movement_direction == DIRECTION_FORWARD) {
    train->movement_direction = DIRECTION_BACKWARD;
  } else {
    train->movement_direction = DIRECTION_FORWARD;
  }

  Printf(uart_tid, 2, "\033[%d;%dHReversed train %d direction to %d", 2, 50, train->id, train->movement_direction);
  TrainControlRequest request = {TRAINCONTROL_TR, train->id, TRAIN_SPEED_REVERSE, delay};
  send_delayed_track_command(&request);
}

void navigate_to_random_exit(int uart_tid, int clock_tid, Train* train, int delay, track_node* current_exit_node, track_node* track){
  if(!current_exit_node) return;
  int current_exit_index = find_track_node_id_by_name(track, current_exit_node->name);
  if(current_exit_index<0) return;
  int current_exit_id = get_exit_id_from_node_index(current_exit_index);

  int exit_node_id = pick_random_exit_id(clock_tid, current_exit_id);
  if(exit_node_id<0 || exit_node_id>9) return;
  int destination_node_index = get_exit_node_index_from_id(exit_node_id);
  Printf(uart_tid, 2, "\033[%d;%dHRandom exit %d, %d", 45, 2, exit_node_id, destination_node_index);
  TrainControlRequest request = {TRAINCONTROL_MV, train->id, train->speed, delay, destination_node_index};
  send_delayed_track_command(&request);
}

inline int get_train_stats_column(Train* train) {
  return TRAIN_STATS_COLUMN + STATS_COLUMN_WIDTH * train->index;
}

void print_train_path(Train* train, int uart_tid) {
  volatile int i=0, j=0, direction=0;

  for(i=0;i<TRACK_MAX;i++) {
    if(&(train->path[i]) == train->pathstart) break;
  }
  while(1) {
    if(i>=TRACK_MAX) break;
    if(train->path[i].node) {
      direction = train->path[i].direction;
      Printfrect(uart_tid, 2, j+33, 2, 120, 0,
        "Next node:%s, direction: %d, dist next node: %d, dist end path: %d, dist sensor: %d, dist BR: %d",
        train->path[i].node->name, direction,
        train->path[i].node->edge[direction].dist, train->path[i].distance_to_end,
        train->path[i].distance_to_next_sensor, train->path[i].distance_to_next_branch);

      if(train->path[i].node == train->destination_node) break;
      i++; j++;
    }
  }
}

int is_sensor_on_path_within_distance(int uart_tid, Train* train, track_node* sensor_node, int distance) {
  int is_sensor_on_path = 0, distance_to_sensor = 0;
  if(!sensor_node || !train->pathstart || !train->current_sensor_on_path) return 0;

  TrackPath *sensor_on_path = train->current_sensor_on_path;

  while(sensor_on_path) {
    if(sensor_on_path->node == sensor_node || sensor_on_path->node == sensor_node->reverse) {
      is_sensor_on_path = 1;
      break;
    }

    if(distance_to_sensor > distance) break;

    distance_to_sensor += sensor_on_path->distance_to_next_sensor;
    sensor_on_path = find_track_node_on_path(train, sensor_on_path->next_sensor);
  }

  return is_sensor_on_path;
}

// iterates over paths of both trains,
// and identifies whether there are sensors on their paths that are going to be hit by both trains soon.
// this can happen when paths of 2 trains are about to be merged together.
// the search forward is limited by trains stopping distances.
int is_other_train_on_merge_path(int uart_tid, Train* train, Train* other_train) {
  int is_other_train_on_merge_path = 0;
  if(!train->pathstart || !train->current_sensor_on_path || !train->velocity ||
     !other_train->pathstart || !other_train->current_sensor_on_path || !other_train->velocity) return 0;

  int distance_to_sensor1 = 0, distance_to_sensor2 = 0;
  int stopping_distance1 = train_get_stopping_distance(train) + 420; // + 420 mm for safety
  int stopping_distance2 = train_get_stopping_distance(other_train) + 420;
  if(stopping_distance1 < 800) stopping_distance1 = 800;
  if(stopping_distance2 < 800) stopping_distance2 = 800;
  TrackPath *sensor_on_path1 = train->current_sensor_on_path, *sensor_on_path2 = other_train->current_sensor_on_path;

  while(sensor_on_path1) {
    sensor_on_path2 = other_train->current_sensor_on_path;
    distance_to_sensor2 = 0;
    while(sensor_on_path2) {

      if(sensor_on_path1->node == sensor_on_path2->node) {
        is_other_train_on_merge_path = 1;
        break;
      }

      if(distance_to_sensor2 > stopping_distance2) break;
      distance_to_sensor2 += sensor_on_path2->distance_to_next_sensor;
      sensor_on_path2 = find_track_node_on_path(other_train, sensor_on_path2->next_sensor);
    }

    if(distance_to_sensor1 > stopping_distance1 || is_other_train_on_merge_path) break;
    distance_to_sensor1 += sensor_on_path1->distance_to_next_sensor;
    sensor_on_path1 = find_track_node_on_path(train, sensor_on_path1->next_sensor);
  }

  return is_other_train_on_merge_path;
}

void process_collision_avoidance(int uart_tid, Train *train, Train *other_trains, track_node* track) {
  if(!train->current_sensor_on_path) return;
  int i, collision_dist = 0, front_to_front_collision_possible = 0, merge_collision_possible = 0;
  Train *other_train = 0;
  int train_speed1 = 0, train_speed2 = 0;

  for (i = 0; i < TOTAL_TRAINS; ++i) {
    other_train = &other_trains[i];
    if(train == other_train || !other_train->current_sensors_edge.src) continue;
    collision_dist = train_get_stopping_distance(train) + train_get_stopping_distance(other_train);
    front_to_front_collision_possible = is_sensor_on_path_within_distance(uart_tid, train, other_train->current_sensors_edge.src, collision_dist);
    merge_collision_possible = is_other_train_on_merge_path(uart_tid, train, other_train);

    if((front_to_front_collision_possible || merge_collision_possible) && train->velocity > 0 && train->avoid_collisions) {
      train_speed1 = train->speed, train_speed2 = other_train->speed;
      TrainControlRequest stop1 = { TRAINCONTROL_TR, train->id, 0, 0 },
                          stop2 = { TRAINCONTROL_TR, other_train->id, 0, 0 };
      send_delayed_track_command(&stop1);
      send_delayed_track_command(&stop2);

      if(front_to_front_collision_possible) {
        Printfrect(uart_tid, 2, 2, 3, STATS_COLUMN_WIDTH*3, 0,
            "Train %d, tracks blocked. Other train: %d", train->id, other_train->id);
        train_reverse_direction(uart_tid, train, 70); // delay 700 ms
      } else if (merge_collision_possible) {
        Printfrect(uart_tid, 2, 2, 3, STATS_COLUMN_WIDTH*3, 0,
            "Train %d, merge collision possible. Other train: %d", train->id, other_train->id);
      }

      // disabling collision logic for 10 seconds to let the train get away. not too pretty, but works
      // re-enabled by sending TRAINCONTROL_AVOID_COL to track server
      train->avoid_collisions = 0;

      if(train->destination_node) {
        int destination_node_id = find_track_node_id_by_name(track, train->destination_node->name);
        TrainControlRequest run1 = { TRAINCONTROL_MV, train->id, train_speed1, 200, destination_node_id}; // delay 2000ms
        send_delayed_track_command(&run1);

        TrainControlRequest command1 = { TRAINCONTROL_AVOID_COL, train->id, 0, 1000}; // delay 10sec
        send_delayed_track_command(&command1);
      }

      if(other_train->destination_node) {
        int destination_node_id2 = find_track_node_id_by_name(track, other_train->destination_node->name);
        TrainControlRequest run2 = { TRAINCONTROL_MV, other_train->id, train_speed2, 1000, destination_node_id2}; // delay 10sec
        send_delayed_track_command(&run2);
        train_erase_path(other_train);
      }
    }
  }
}

void train_process_sensor_timeout(int uart_tid, track_node *track, Train *train, track_node* sensor_node) {
  char* no_node_name = "0";
  if(!train || !train->pathstart) return;
  int timeout_sensor_index_on_path = -1, current_sensor_index_on_path = -1;

  if(train->current_sensor_on_path) {
    timeout_sensor_index_on_path = find_track_node_index_on_path(train, sensor_node);
    current_sensor_index_on_path = find_track_node_index_on_path(train, train->current_sensor_on_path->node);

    if(current_sensor_index_on_path < timeout_sensor_index_on_path) {
      train->sensor_timeouts_count++;
      Printfrect(uart_tid, 2, 23, get_train_stats_column(train), STATS_COLUMN_WIDTH, 0,
        "Sensor timeout: %s", (sensor_node ? sensor_node->name : no_node_name));
      // if(train->sensor_timeouts_count >=2) {
      //   train->sensor_timeouts_count = 0;
      //   print_train_is_off_path(uart_tid, train);
      //   correct_path_when_off_track(uart_tid, train, track);
      // }
    }
  }
}

// initial velocity estimates, MUST be adjusted dynamically
// depending on actual train velocity
// (distance_timings measured in 983kHz ticks for C13_E7 distance: 785 mm)
void setup_train(Train* train, int train_id) {
  train->velocity = 0;
  train->speed = 0;
  train->prev_speed = 0;
  train->pathstart = 0;
  train->destination_node = 0;
  track_edge current_sensors_edge = {0, 0, 0, 0}, prev_sensors_edge = {0, 0, 0, 0};
  train->current_sensors_edge = current_sensors_edge;
  train->prev_sensors_edge = prev_sensors_edge;
  train->estimated_time_current_sensors_edge = 0;
  train->estimated_time_prev_sensors_edge = 0;
  train->actual_time_current_sensor = 0;
  train->actual_time_prev_sensor = 0;
  train->actual_time_prev_sensors_edge = 0;
  train->prev_sensors_edge_time_estimate_error = 0;
  train->movement_direction = DIRECTION_FORWARD;
  train->stop_command_sent = 0;
  train->navigation_paths_count = 0;
  train->sensor_timeouts_count = 0;

  train->prev_sensor_on_path = 0;
  train->current_sensor_on_path = 0;
  train->next_sensor_on_path = 0;
  train->second_next_sensor_on_path = 0;

  train->next_sensor_on_path_distance = 0;
  train->next_sensor_on_path_eta = 0;
  train->second_next_sensor_on_path_distance = 0;
  train->second_next_sensor_on_path_eta = 0;

  train->demo_id = 0;
  train->avoid_collisions = 1;

  if(train && train_id == 71) {
    train->id = 71;
    train->index = 0;
    train->timings[0]  = 0;
    train->timings[1]  = 0;
    train->timings[2]  = 0;
    train->timings[3]  = 0;
    train->timings[4]  = 13961165;
    train->timings[5]  = 12331010;
    train->timings[6]  = 6716007;
    train->timings[7]  = 4723738;
    train->timings[8]  = 3697203;
    train->timings[9]  = 2912344;
    train->timings[10] = 2610561;
    train->timings[11] = 2157722;
    train->timings[12] = 1825690;
    train->timings[13] = 1584129;
    train->timings[14] = 1342641;

    train->stopping_distance[0]  = 0;
    train->stopping_distance[1]  = 0;
    train->stopping_distance[2]  = 0;
    train->stopping_distance[3]  = 0;
    train->stopping_distance[4]  = 15;
    train->stopping_distance[5]  = 35;
    train->stopping_distance[6]  = 70;
    train->stopping_distance[7]  = 100;
    train->stopping_distance[8]  = 160;
    train->stopping_distance[9]  = 250;
    train->stopping_distance[10] = 365;
    train->stopping_distance[11] = 490;
    train->stopping_distance[12] = 670;
    train->stopping_distance[13] = 880;
    train->stopping_distance[14] = 1160;
  }

  if(train && train_id == 69) {
    train->id = 69;
    train->index = 1;
    train->timings[0]  = 0;
    train->timings[1]  = 0;
    train->timings[2]  = 10096974;
    train->timings[3]  = 6051891;
    train->timings[4]  = 4361498;
    train->timings[5]  = 3335143;
    train->timings[6]  = 2731393;
    train->timings[7]  = 2308608;
    train->timings[8]  = 2067201;
    train->timings[9]  = 1825613;
    train->timings[10] = 1644493;
    train->timings[11] = 1523764;
    train->timings[12] = 1402983;
    train->timings[13] = 1282330;
    train->timings[14] = 1282330;

    train->stopping_distance[0]  = 0;
    train->stopping_distance[1]  = 0;
    train->stopping_distance[2]  = 45;
    train->stopping_distance[3]  = 130;
    train->stopping_distance[4]  = 205;
    train->stopping_distance[5]  = 270;
    train->stopping_distance[6]  = 340;
    train->stopping_distance[7]  = 440;
    train->stopping_distance[8]  = 485;
    train->stopping_distance[9]  = 565;
    train->stopping_distance[10] = 630;
    train->stopping_distance[11] = 670;
    train->stopping_distance[12] = 765;
    train->stopping_distance[13] = 825;
    train->stopping_distance[14] = 825;
  }

  if(train && train_id == 63) {
    train->id = 63;
    train->index = 2;
    train->timings[0]  = 0;
    train->timings[1]  = 0;
    train->timings[2]  = 10258119;
    train->timings[3]  = 5810465;
    train->timings[4]  = 4301041;
    train->timings[5]  = 3258588;
    train->timings[6]  = 2642711;
    train->timings[7]  = 2216069;
    train->timings[8]  = 1938311;
    train->timings[9]  = 1721002;
    train->timings[10] = 1539879;
    train->timings[11] = 1370814;
    train->timings[12] = 1258112;
    train->timings[13] = 1161518;
    train->timings[14] = 1161518;

    train->stopping_distance[0]  = 0;
    train->stopping_distance[1]  = 5;
    train->stopping_distance[2]  = 70;
    train->stopping_distance[3]  = 150;
    train->stopping_distance[4]  = 230;
    train->stopping_distance[5]  = 300;
    train->stopping_distance[6]  = 377;
    train->stopping_distance[7]  = 470;
    train->stopping_distance[8]  = 540;
    train->stopping_distance[9]  = 590;
    train->stopping_distance[10] = 670;
    train->stopping_distance[11] = 763;
    train->stopping_distance[12] = 935;
    train->stopping_distance[13] = 1020;
    train->stopping_distance[14] = 1020;
  }

  if(train && train_id == 58) {
    train->id = 58;
    train->index = 3;
    train->timings[0]  = 0;
    train->timings[1]  = 0;
    train->timings[2]  = 0;
    train->timings[3]  = 0;
    train->timings[4]  = 0;
    train->timings[5]  = 11757419;
    train->timings[6]  = 7544082;
    train->timings[7]  = 4510049;
    train->timings[8]  = 3425609;
    train->timings[9]  = 2708629;
    train->timings[10] = 2228153;
    train->timings[11] = 1829673;
    train->timings[12] = 1576096;
    train->timings[13] = 1463383;
    train->timings[14] = 1269337;

    train->stopping_distance[0]  = 0;
    train->stopping_distance[1]  = 0;
    train->stopping_distance[2]  = 0;
    train->stopping_distance[3]  = 0;
    train->stopping_distance[4]  = 0;
    train->stopping_distance[5]  = 0;
    train->stopping_distance[6]  = 0;
    train->stopping_distance[7]  = 125;
    train->stopping_distance[8]  = 185;
    train->stopping_distance[9]  = 300;
    train->stopping_distance[10] = 400;
    train->stopping_distance[11] = 535;
    train->stopping_distance[12] = 730;
    train->stopping_distance[13] = 1020;
    train->stopping_distance[14] = 1300;
  }
}
