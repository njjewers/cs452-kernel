#ifndef __TRAIN_CONTROL_SERVER_H__
#define __TRAIN_CONTROL_SERVER_H__

#include <train.h>

#define SWITCHES_ON_TRACK 22 // catch 22! ;)
#define SWITCH_STRAIGHT 33
#define SWITCH_CURVED 34
#define SOLENOID_OFF 32

#define MEASUREMENTS_MAX 10

typedef enum {
  TRAINCONTROL_TR,
  TRAINCONTROL_LOC,
  TRAINCONTROL_MV,
  TRAINCONTROL_RV,
  TRAINCONTROL_SW,
  TRAINCONTROL_DEMO,
  TRAINCONTROL_SOLENOID_OFF,
  TRAINCONTROL_SENSOR_DUMP,
  TRAINCONTROL_SENSOR_TIMEOUT,
  TRAINCONTROL_AVOID_COL,
} TrainControlRequestType;

typedef struct {
  TrainControlRequestType type;
  int target;
  int arg;
  int delay; // in 10 ms ticks
  int arg2;
} TrainControlRequest;

// --- sensors ---

#define TOTAL_SENSORS 5
#define SINGLE_SENSOR_DUMP 0xC0 // 192 decimal
#define MULTIPLE_SENSORS_DUMP 0x80 // 128 decimal

/*
 * The s88 units can be told to reset its memory after a dump to the computer,
 * or it can remain as it was and continue adding data to the byte. 
 * A reset would make each position in memory a "0" again. 
 * If you want it to reset, send the single byte 0xC0 without adding any numbers to it. 
 * If the reset mode is to be "off", then send the code 0x80.
 */
#define RESET_SENSORS_AFTER_DUMP 0xC0
#define NO_SENSORS_RESET_AFTER_DUMP 0x80

#define SENSOR_A 0
#define SENSOR_B 1
#define SENSOR_C 2
#define SENSOR_D 3
#define SENSOR_E 4

void train_control_server();

void send_delayed_track_command(TrainControlRequest * request);
int switch_id_to_index(int switch_id);
char switch_command_id_to_command_name(int command_id);
int is_a_center_switch(int switch_id);
int get_opposite_center_switch(int switch_id);
void set_switch_position(int uart_tid, int* switches, int switch_id, int position, int set_solenoids_off);
void reverse_train(int uart_tid, Train* train);

#endif
