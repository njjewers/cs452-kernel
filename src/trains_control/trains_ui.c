#include "trains_ui.h"
#include "trains_user_input.h"
#include "train.h"
#include "user_api.h"
#include "user_system_tasks.h"
#include "util_ringbuffer.h"
#include "train_control_server.h"

#define STRINGIZE2(s) #s
#define STRINGIZE(s) STRINGIZE2(s)
#define FRAME_HEIGHT_STR STRINGIZE(FRAME_HEIGHT)
#define FRAME_WIDTH_STR STRINGIZE(FRAME_WIDTH)

// Forward-declare drawing functions
void draw_frame(int uart_tid);
void print_newlines(int uart_tid, int rows);
void toggle_cursor(int uart_tid, int show);
void print_timer(int uart_tid, int time);
void print_idle_task_running_time(int uart_tid, int idle_task_tid);
void print_prompt(int uart_tid);
void print_task_count(int uart_tid);
void clear_error_message(int uart_tid);

void trains_ui_server() {
  RegisterAs(TRAINS_UI_SERVICE);

  int clock_tid = WhoIs(CLOCKSERVICE_NAME);
  int uart_tid = WhoIs(UARTSERVICE_NAME);
  // WhoIs(IDLETASK_NAME); idle task priority = 0, hasn't yet registered with NameService
  int idle_task_tid = 0x40004;

  draw_frame(uart_tid);
  toggle_cursor(uart_tid, 0);
  int time;

  FOREVER {
    Delay(clock_tid, 10); // wake up each 100ms
    time = Time(clock_tid);
    print_timer(uart_tid, time);
    print_idle_task_running_time(uart_tid, idle_task_tid);
    print_task_count(uart_tid);
    if(time % 500 == 0) clear_error_message(uart_tid);
  }
  Exit();
}

void draw_frame(int uart_tid) {
  int i;
  print_newlines(uart_tid, FRAME_HEIGHT*2);
  clear_screen(uart_tid);
  Printf(uart_tid, 2, "\033[H");
  for(i=0; i<FRAME_WIDTH; i++) {
    Putc(uart_tid, 2, '#');  
  }
  for(i=2; i<FRAME_HEIGHT; i++) {
    Printf(uart_tid, 2, "\033[%d;0H#\033[%dC#", i, FRAME_WIDTH-2); 
  }
  Putstr(uart_tid, 2, "\033["FRAME_HEIGHT_STR";0H");
  for(i=0; i<FRAME_WIDTH; i++) {
    Putc(uart_tid, 2, '#');  
  }
  print_prompt(uart_tid);
}

void print_newlines(int uart_tid, int rows) {
  int i;
  for (i=0; i<rows; i++) {
    Putc(uart_tid, 2, '\n');  
  }
}

void clear_screen(int uart_tid) {
  Printf(uart_tid, 2, "\033[2J");
};

void toggle_cursor(int uart_tid, int show) {
  show ? Printf(uart_tid, 2, "\033[?25h") : Printf(uart_tid, 2, "\033[?25l");
}

void print_prompt(int uart_tid) {
  Printf(uart_tid, 2, "\033[%d;%dHEnter command > ", FRAME_HEIGHT-2, 3);
}

void print_timer(int uart_tid, int time) { // 10ms timeticks
  Printfrect(uart_tid, 2, 4, STATS_COLUMN, STATS_COLUMN, 0, 
    "Time: %t", time);
}

void print_idle_task_running_time(int uart_tid, int idle_task_tid) {
  int idle_running_time_percentage = RunningTime(idle_task_tid, 1);

  int idle_running_time_whole = idle_running_time_percentage / 100;
  int idle_running_time_decimals = idle_running_time_percentage - idle_running_time_whole * 100;

  Printfrect(uart_tid, 2, 5, STATS_COLUMN, STATS_COLUMN, 0, 
    "CPU idle: %d.%d%%", 
    idle_running_time_whole, idle_running_time_decimals);
}

void print_task_count(int uart_tid) {
  Printfrect(uart_tid, 2, 6, STATS_COLUMN, STATS_COLUMN, 0,
    "Task count: %d", 
    TaskCount());
}

void print_current_sensor(int uart_tid, track_node* sensor_node) {
  if(!sensor_node) return;
  char* no_node_name = "0";
  Printfrect(uart_tid, 2, 7, STATS_COLUMN, STATS_COLUMN, 0, "Triggered sensor: %s\n",
      (sensor_node ? sensor_node->name : no_node_name));
}

void print_train_stats(int uart_tid, Train* train) {
  char* no_node_name = "0";

  Printfrect(uart_tid, 2, 4, get_train_stats_column(train), STATS_COLUMN_WIDTH, 0,
    "Train %d: %d mm/sec\n"
    "Triggered sensor: %s\n"
    "Next sensor: %s\n"
    "Distance to next: %u mm\n"
    "Est. next sensor: %d ms\n"
    "Actual prev path: %d ms\n"
    "Est. prev path: %d ms\n"
    "Prev path error: %d ms",
    train->id, (int) train->velocity,
    (train->current_sensors_edge.src ? train->current_sensors_edge.src->name : no_node_name),
    (train->current_sensors_edge.dest ? train->current_sensors_edge.dest->name : no_node_name), 
    train->current_sensors_edge.dist,
    train->estimated_time_current_sensors_edge,
    train->actual_time_prev_sensors_edge,
    train->estimated_time_prev_sensors_edge,
    train->prev_sensors_edge_time_estimate_error);
}

void clear_error_message(int uart_tid) {
  Printf(uart_tid, 2, "\033[%d;%dH                                                                                        ", 2, 3);
}
