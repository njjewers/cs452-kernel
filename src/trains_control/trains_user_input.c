#include "trains_user_input.h"
#include "train_control_server.h"
#include "string.h"
#include "trains_ui.h"
#include "user_api.h"
#include "user_system_tasks.h"
#include "user_tasks.h"
#include "track_pathfinding.h"

// Forward-declare utility functions
static void print_previous_command(int uart_tid, char* input_command);
static void print_active_command(int uart_tid, char* input_command);

static int parse_command(int uart_tid, char* input_command, int trains_control_tid, track_node* track);
static void get_string_part(char* input_string,  int part, char* delim, char* string_part);
static int find_delim_index( char* string,  char* delim,  int occurence);
static int switch_command_name_to_command_id(char* command_name);

#define INPUT_BUFFER_SIZE 40

void trains_user_input_server() {
  RegisterAs(TRAINS_USER_INPUT_SERVICE);
  
  int uart_tid = WhoIs(UARTSERVICE_NAME);
  int trains_control_tid = WhoIs(TRAINS_CONTROL_SERVICE);

  // Initializing the track both in trains_user_input_server() and train_control_server()
  // Shouldn't be a problem as long as track graph is only read-only data
  track_node track[TRACK_MAX]; init_tracka(track);

  char c, input_command[INPUT_BUFFER_SIZE];
  memset(input_command, '\0', INPUT_BUFFER_SIZE);

  char *input_active = input_command;

  for(;;) {
    c = Getc(uart_tid, 2);
    
    int result = 0;
    if (c == 3) { // ctrl-c
      result = -1;
    } else if (c == 8 || c == 127) { // Backspace, ascii DEL
      if (input_active != input_command) {
        *(--input_active) = '\0';
      } 
    } else if(c == 13) { // Carriage return
      print_previous_command(uart_tid, input_command);
      result = parse_command(uart_tid, input_command, trains_control_tid, track);
      memset(input_command, '\0', INPUT_BUFFER_SIZE);
      input_active = input_command;
    } else {
      if (input_active - input_command < (INPUT_BUFFER_SIZE - 1)) {
        *input_active++ = c;
      }
    }

    print_active_command(uart_tid, input_command);
    
    if(result == -1) {
      Create(10, quit_all_tasks);
    }
  }
  Exit();
}

static void print_previous_command(int uart_tid, char* input_command) {
  Printfrect(uart_tid, 2, FRAME_HEIGHT-3, 19, INPUT_BUFFER_SIZE + 1, 0, "%s", input_command);
}

static void print_active_command(int uart_tid, char* input_command) {
  Printfrect(uart_tid, 2, FRAME_HEIGHT-2, 19, INPUT_BUFFER_SIZE + 1, 0, "%s", input_command);
}

static int parse_command(int uart_tid, char* input_command, int trains_control_tid, track_node* track) {
  int result_status = 0, train_id = 0, train_speed = 0, switch_id = 0, switch_position = 0, track_node_id = 0, demo_id = 0;
  char *train_forward_command = "tr", *train_reverse_command = "rv", *train_move_command = "mv", *train_location_command = "loc",
     *switch_control_command = "sw", *demo_command = "demo", *quit_command = "q", *delim = " ";
  char command[COMMAND_PARTS][10]; //three command parts, 10 char max length

  int i;
  for (i=0; i<COMMAND_PARTS; i++) {
    get_string_part(input_command, i, delim, command[i]);
  }

  if (strcmp(command[0], train_forward_command) == 0) {
    train_id = atoi(command[1]);
    train_speed = atoi(command[2]);
    TrainControlRequest request = {TRAINCONTROL_TR, train_id, train_speed};
    Send(trains_control_tid, (char *) &request, sizeof(request), 0, 0);
  } else if (strcmp(command[0], train_location_command) == 0) {
    train_id = atoi(command[1]);
    track_node_id = find_track_node_id_by_name(track, command[2]);
    if (track_node_id == -1) {
      Printf(uart_tid, 2, "\033[%d;%dHCouldn't find %s on track.", 2, 3, command[2]);
      return 0;
    }
    TrainControlRequest request = {TRAINCONTROL_LOC, train_id, track_node_id, 0};
    Send(trains_control_tid, (char *) &request, sizeof(request), 0, 0);
  } else if (strcmp(command[0], train_move_command) == 0) {
    train_id = atoi(command[1]);
    train_speed = atoi(command[3]);
    track_node_id = find_track_node_id_by_name(track, command[2]);
    if (track_node_id == -1) {
      Printf(uart_tid, 2, "\033[%d;%dHCouldn't find %s on track.", 2, 3, command[2]);
      return 0;
    }
    TrainControlRequest request = {TRAINCONTROL_MV, train_id, train_speed, 0, track_node_id};
    Send(trains_control_tid, (char *) &request, sizeof(request), 0, 0);
  } else if (strcmp(command[0], train_reverse_command) == 0) {
    train_id = atoi(command[1]);
    TrainControlRequest request = {TRAINCONTROL_RV, train_id, 0};
    Send(trains_control_tid, (char *) &request, sizeof(request), 0, 0);
  } else if (strcmp(command[0], switch_control_command) == 0) {
    switch_id = atoi(command[1]);
    switch_position = switch_command_name_to_command_id(command[2]);
    TrainControlRequest request = {TRAINCONTROL_SW, switch_id, switch_position};
    Send(trains_control_tid, (char *) &request, sizeof(request), 0, 0);
  } else if (strcmp(command[0], demo_command) == 0) {
    train_id = atoi(command[1]);
    demo_id = atoi(command[2]);
    TrainControlRequest request = {TRAINCONTROL_DEMO, train_id, demo_id, 0};
    Send(trains_control_tid, (char *) &request, sizeof(request), 0, 0);
  } else if (strcmp(command[0], quit_command) == 0) {
    result_status = -1;
  }
  return result_status;
}

static void get_string_part(char* input_string,  int part, char* delim, char* string_part) {
  int start_index = 0;
  int end_index = 0;
  short int size = 0;

  if(!part == 0)
    start_index = find_delim_index(input_string, delim, part-1) + 1;
  end_index = find_delim_index(input_string, delim, part);

  size = end_index-start_index;
  if(size <= 0) return; // command doesn't have n-th part ("rv 70" only has 2 parts, and won't find a 3rd part).
  if(size > 9) size = 9; // command parts shouldn't exceed 10 chars
  memcpy(string_part, &input_string[start_index], size);
  string_part[size] = 0x00;
}

static int find_delim_index( char* string,  char* delim,  int occurence) {
  int delim_occurence = 0; //for ex., find a second occurence of " "
  int position_index = 0;
  while(*string) {
    if(*string == *delim) {
      if(delim_occurence == occurence) return position_index;
      delim_occurence++;
    }
    position_index++;
    string++;
  }
  return position_index; //not found, return this string's null-termination char position
}

// straight (S=33), curved (C=34)
static int switch_command_name_to_command_id(char* command_name) {
  int switch_command_id = 33;
  if      (strcmp(command_name, "S") == 0 || strcmp(command_name, "s") == 0) switch_command_id = 33;
  else if (strcmp(command_name, "C") == 0 || strcmp(command_name, "c") == 0) switch_command_id = 34;
  return switch_command_id;
}
