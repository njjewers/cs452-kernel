#ifndef __TRAINS_UI_H__
#define __TRAINS_UI_H__

#include "track_node.h"
#include "train.h"

#define FRAME_WIDTH 190
#define FRAME_HEIGHT 30

#define STATS_COLUMN 22
#define TRAIN_STATS_COLUMN 50
#define STATS_COLUMN_WIDTH 35

void trains_ui_server();
void clear_screen(int uart_tid);
void print_current_sensor(int uart_tid, track_node* sensor_node);
void print_train_stats(int uart_tid, Train* train);

#endif
