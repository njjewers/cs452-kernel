#ifndef __TRAIN_H__
#define __TRAIN_H__

#include "track_node.h"
#include "track_pathfinding.h"

#define SPEED_MAX 15
#define TRAIN_SPEED_STOP 0
#define TRAIN_SPEED_REVERSE 15

#define TIMER4_SEC_RATIO (((float) 1/983) / 1000) // 983 kHz
#define TIMER4_MS_RATIO   ((float) 1/983) // 983 kHz
#define C13_E7_TRACK_B_DISTANCE 785 // mm

#define TOTAL_TRAINS 4

#define DIRECTION_FORWARD 0
#define DIRECTION_BACKWARD 1

#define TRAIN_LENGTH 215 // mm
#define TRAIN_LENGTH_TILL_SENSOR_HOOK 45 //mm

#define TRAIN_SENSOR_ATTRIB_TIMEOUT_MS 1000 //ms

typedef struct {
  int id; // train number: 69, 71, 58, etc...
  int index; //sequential number: 0, 1, 2, etc...
  float velocity; // mm/sec
  int location; // on current edge, in mm
  track_edge prev_sensors_edge, current_sensors_edge;
  int estimated_time_current_sensors_edge, estimated_time_prev_sensors_edge; // in ms
  int actual_time_current_sensor, actual_time_prev_sensor, actual_time_prev_sensors_edge; // in ms
  int prev_sensors_edge_time_estimate_error; //in ms
  int timings[SPEED_MAX];
  int stopping_distance[SPEED_MAX]; // in mm
  int speed, prev_speed;
  TrackPath path[TRACK_MAX], *pathstart;
  TrackPath *prev_sensor_on_path, *current_sensor_on_path, *next_sensor_on_path, *second_next_sensor_on_path;
  int next_sensor_on_path_distance, second_next_sensor_on_path_distance;
  int next_sensor_on_path_eta, second_next_sensor_on_path_eta; //in ms from beginning of epoch
  track_node* destination_node;
  int movement_direction;
  int stop_command_sent;
  int navigation_paths_count;
  int sensor_timeouts_count;

  int demo_id;
  int avoid_collisions;
} Train;

void setup_train(Train* train, int train_id);
Train* get_train(Train* trains, int train_id);
void set_train_velocity(Train* train, int speed);
float get_velocity_for_speed(Train* train, int speed);
int get_time_for_distance(Train* train, int distance);
void train_adjust_velocity(int uart_tid, int clock_tid, Train* train, track_node* track, int switches[]);
void train_adjust_path(int uart_tid, int clock_tid, Train* train, track_node* current_node, track_node* track, int switches[], 
    Train *other_trains);
void flip_all_switches_for_path(int uart_tid, Train* train, int switches[]);
void flip_switch_on_path(int uart_tid, int switches[], Train* train, track_node* node, int delay);
void print_train_path(Train* train, int uart_tid);
void train_erase_path(Train* train);
void reverse_current_sensors_edge(Train* train);
inline int get_train_stats_column(Train* train);
void train_process_sensor_timeout(int uart_tid, track_node *track, Train *train, track_node* sensor_node);

#endif
