#include "train_control_server.h"
#include "user_api.h"
#include "trains_ui.h"
#include "ts7200.h"
#include "track_data.h"
#include "track_node.h"
#include "train.h"
#include "track_pathfinding.h"

//Track-wide control methods
void init_track(int uart_tid, int* switches);
void start_track(int uart_tid);
void stop_track(int uart_tid);

// Train control methods
void set_train_speed(int uart_tid, Train* train, int speed);
void stop_train(int uart_tid, Train* train);
void move_train_to_dest(int uart_tid, track_node* track, Train* train, track_node* destination_node, int speed, int switches[]);
Train* attribute_sensor_node_to_train(int clock_tid, Train trains[], track_node* current_sensor_node);
void train_assign_sensor_passed(int uart_tid, int clock_tid, track_node* track, int switches[],
    Train* train, track_node* sensor_node, Train *other_trains);
void run_demo_scenario(int uart_tid, track_node* track, Train *trains, Train* train, int demo_id);

// Switch control and display methods
void init_switches(int uart_tid, int* switches);
void deactivate_solenoids_delayed(int delay);
void deactivate_solenoids(int uart_tid);
int switch_index_to_id(int switch_index);
int switch_index_to_branch_node_index(int switch_index);
void print_switches(int uart_tid, int* switches);
void print_switch(int uart_tid, int* switches, int switch_index);

// Sensors methods
void sensor_listener_task();
void init_sensors(int uart_tid);
track_node* get_sensor_node_from_data_byte(char* c, int sensors_bytes_received, track_node *track);
int get_sensor_node_index(int sensor_decoder_id, int sensor_index);
int get_sensor_index_from_data_byte(char* c, int byte_id);
int is_sensor_bit_set(int bit_position, char* c);
int get_sensor_decoder_id_from_data_byte(int sensors_bytes_received);
void set_edge_to_next_sensor_node(track_node* current_sensor_node, track_edge* edge, int switches[]);
int track_edges_equal(track_edge* lhs, track_edge* rhs);

void measure_velocity(int uart_tid, Train* train, track_node *current_sensor_node,
    int distance_measurements[], int* start_time, int* finish_time, int* distance_measurement_id,
    int start_time_measurements[], int* start_time_measurement_id, 
    int finish_time_measurements[], int* finish_time_measurement_id);

void train_control_server() {
  RegisterAs(TRAINS_CONTROL_SERVICE);

  track_node track_a[TRACK_MAX]; init_tracka(track_a);
  track_node track_b[TRACK_MAX]; init_trackb(track_b);
  track_node *track = track_a;
  track_node *current_sensor_node=0, *destination_node=0;
  int tid;

  Train trains[TOTAL_TRAINS];
  setup_train(&trains[0], 71);
  setup_train(&trains[1], 69);
  setup_train(&trains[2], 63);
  setup_train(&trains[3], 58);
  Train* train=0;

  TrainControlRequest request;
  int uart_tid = WhoIs(UARTSERVICE_NAME);
  int clock_tid = WhoIs(CLOCKSERVICE_NAME);

  int switches[SWITCHES_ON_TRACK];
  init_track(uart_tid, switches);
  init_sensors(uart_tid);

  Create(10, sensor_listener_task);


  // This loop shouldn't be blocked with Delay(), and should
  // always be ready to receive and deliver a command to the track
  FOREVER {
    Receive(&tid, (char*)&request, sizeof(request));
    Reply(tid, (void*)0, 0);

    if(request.type == TRAINCONTROL_TR || request.type == TRAINCONTROL_MV ||
       request.type == TRAINCONTROL_RV || request.type == TRAINCONTROL_LOC ||
       request.type == TRAINCONTROL_SENSOR_TIMEOUT || request.type == TRAINCONTROL_DEMO ||
       request.type == TRAINCONTROL_AVOID_COL) {
      train = get_train(trains, request.target);
      if (!train) {
        Printf(uart_tid, 2, "\033[%d;%dHCouldn't find train %d.", 2, 3, request.target);
        continue;
      }
    }

    switch (request.type) {
      case TRAINCONTROL_TR:
        set_train_speed(uart_tid, train, request.arg);
      break;
      case TRAINCONTROL_LOC:
        current_sensor_node = &track[request.arg]; //error-checking in parse_command()
        train_erase_path(train);
        train_assign_sensor_passed(uart_tid, clock_tid, track, switches, train, current_sensor_node, trains);
      break;
      case TRAINCONTROL_MV:
        destination_node = &track[request.arg2]; //error-checking in parse_command()
        move_train_to_dest(uart_tid, track, train, destination_node, request.arg, switches);
        destination_node = 0;
      break;
      case TRAINCONTROL_RV:
        reverse_train(uart_tid, train);
      break;
      case TRAINCONTROL_SW:
        set_switch_position(uart_tid, switches, request.target, request.arg, 1);
      break;
      case TRAINCONTROL_DEMO:
        run_demo_scenario(uart_tid, track, trains, train, request.arg);
      break;
      case TRAINCONTROL_SOLENOID_OFF:
        deactivate_solenoids(uart_tid);
      break;
      case TRAINCONTROL_SENSOR_DUMP:
        current_sensor_node = get_sensor_node_from_data_byte((char*) &request.target, request.arg, track);
        print_current_sensor(uart_tid, current_sensor_node);
        train = attribute_sensor_node_to_train(clock_tid, trains, current_sensor_node);
        if(train) {
          train_assign_sensor_passed(uart_tid, clock_tid, track, switches, train, current_sensor_node, trains);
        }
      break;
      case TRAINCONTROL_SENSOR_TIMEOUT:
        current_sensor_node = &track[request.arg]; //error-checking in parse_command()
        train_process_sensor_timeout(uart_tid, track, train, current_sensor_node);
        current_sensor_node = 0;
      break;
      case TRAINCONTROL_AVOID_COL:
        train->avoid_collisions = 1;
      break;
    }
    train = 0;
  }
}

void sensor_listener_task() {
  int train_control = MyParentTid();
  TrainControlRequest request = {TRAINCONTROL_SENSOR_DUMP, 0, 0};
  int uart_tid = WhoIs(UARTSERVICE_NAME);
  volatile int i = 0;
  volatile int sensors_request_sent = 0;

  FOREVER {
    if(!sensors_request_sent) {
      Putc(uart_tid, 1, MULTIPLE_SENSORS_DUMP+TOTAL_SENSORS);
      sensors_request_sent = 1;
      for(i = 1; i<=TOTAL_SENSORS * 2; i++) {
        request.target = Getc(uart_tid, 1) & 0x000000ff;
        request.arg = i;
        Send(train_control, (char*) &request, sizeof(request), (char*)0, 0);
        if(i == TOTAL_SENSORS * 2) { // each sensor returns 2 bytes
          sensors_request_sent = 0;
        }
      }
    }
  }
}

// Mechanism for delayed commands: Spawn a subprocess
void delayed_track_command() {
  int trains_control_tid = MyParentTid(), tid;
  int clock_server_tid = WhoIs(CLOCKSERVICE_NAME);

  TrainControlRequest request;
  Receive(&tid, (char*)&request, sizeof(request));
  Reply(tid, (void*)0, 0);

  Delay(clock_server_tid, request.delay);
  Send(trains_control_tid, (char *) &request, sizeof(request), 0, 0);
  Exit();
}

void send_delayed_track_command(TrainControlRequest * request) {
  int delayed_command = Create(30, delayed_track_command);
  Send(delayed_command, (char *)request, sizeof(TrainControlRequest), 0, 0);
}

void init_track(int uart_tid, int* switches) {
  start_track(uart_tid);
  init_switches(uart_tid, switches);
}

void start_track(int uart_tid) {
  Putc(uart_tid, 1, 0x60);
}

void stop_track(int uart_tid) {
  Putc(uart_tid, 1, 0x61);
}

// 0=stop, 1=slowest, 14=fastest, 15=reverse direction
void set_train_speed(int uart_tid, Train* train, int speed) {
  if(speed < 0 || speed > 15) speed = 0;
  if(!train) return;

  set_train_velocity(train, speed);

  if(speed == TRAIN_SPEED_REVERSE) {
    reverse_current_sensors_edge(train);
  }

  Putc(uart_tid, 1, speed);
  Putc(uart_tid, 1, train->id);
}

void stop_train(int uart_tid, Train* train) {
  set_train_speed(uart_tid, train, 0);
}

void reverse_train(int uart_tid, Train* train) {
  int orig_speed = train->speed;
  stop_train(uart_tid, train);

  TrainControlRequest request = {TRAINCONTROL_TR, train->id, 15, 200};
  send_delayed_track_command(&request);

  TrainControlRequest request2 = {TRAINCONTROL_TR, train->id, orig_speed, 220};
  send_delayed_track_command(&request2);
}

void move_train_to_dest(int uart_tid, track_node* track, Train* train, track_node* destination_node, int speed, int switches[]){
  char* no_node_name = "0";

  train_erase_path(train);
  train->stop_command_sent = 0;
  train->navigation_paths_count++;
  if(train->current_sensors_edge.src) {
    train->pathstart = track_pathfind(train->path, track, train->current_sensors_edge.src, destination_node);
    Printf(uart_tid, 2, "\033[%d;%dHMoving to %s, sensors edge src %s, dest %s", 40, 2,
      (destination_node ? destination_node->name : no_node_name),
      (train->current_sensors_edge.src ? train->current_sensors_edge.src->name : no_node_name),
      (train->current_sensors_edge.dest ? train->current_sensors_edge.dest->name : no_node_name));

    Printfrect(uart_tid, 2, 22, get_train_stats_column(train), STATS_COLUMN_WIDTH, 0,
        "Train navig count: %d", train->navigation_paths_count);

    train->destination_node = destination_node;
    // flip_all_switches_for_path(uart_tid, train, switches);
    set_train_speed(uart_tid, train, speed);
    // print_train_path(train, uart_tid);
  }
}

void run_demo_scenario(int uart_tid, track_node* track, Train *trains, Train* train, int demo_id) {
  Printfrect(uart_tid, 2, 2, get_train_stats_column(train), STATS_COLUMN_WIDTH, 0,
      "Train %d, Demo scen: %d", train->id, demo_id);

  if(demo_id == 1) {
    train->demo_id = demo_id;
    int exit_node_id = 5;
    int destination_node_index = get_exit_node_index_from_id(exit_node_id);
    Printf(uart_tid, 2, "\033[%d;%dHRandom exit %d, %d", 45, 2, exit_node_id, destination_node_index);
    TrainControlRequest request = {TRAINCONTROL_MV, train->id, 10, 0, destination_node_index};
    send_delayed_track_command(&request);
  } else if (demo_id == 2) {
    train->demo_id = demo_id;
    int track_node_id = find_track_node_id_by_name(track, "B6");
    TrainControlRequest request = {TRAINCONTROL_MV, train->id, 10, 0, track_node_id};
    send_delayed_track_command(&request);
  } else if (demo_id == 3) {
    Train* train1 = get_train(trains, 71);
    Train* train2 = get_train(trains, 58);

    int track_node_id1 = find_track_node_id_by_name(track, "EN4");
    TrainControlRequest request1 = {TRAINCONTROL_LOC, train1->id, track_node_id1, 0};
    send_delayed_track_command(&request1);

    int track_node_id2 = find_track_node_id_by_name(track, "EN10");
    TrainControlRequest request2 = {TRAINCONTROL_LOC, train2->id, track_node_id2, 0};
    send_delayed_track_command(&request2);

    int track_node_id3 = find_track_node_id_by_name(track, "EX10");
    TrainControlRequest request3 = {TRAINCONTROL_MV, train1->id, 9, 50, track_node_id3}; //in 0.5 sec
    send_delayed_track_command(&request3);

    int track_node_id4 = find_track_node_id_by_name(track, "EX4");
    TrainControlRequest request4 = {TRAINCONTROL_MV, train2->id, 9, 250, track_node_id4}; //in 3 sec
    send_delayed_track_command(&request4);
  } else if (demo_id == 4) {
    Train* train1 = get_train(trains, 58);
    Train* train2 = get_train(trains, 71);

    TrainControlRequest request1 = {TRAINCONTROL_SW, 10, 33, 0}; //straight
    send_delayed_track_command(&request1);

    int track_node_id1 = find_track_node_id_by_name(track, "D5");
    TrainControlRequest request2 = {TRAINCONTROL_LOC, train2->id, track_node_id1, 0};
    send_delayed_track_command(&request2);

    int track_node_id2 = find_track_node_id_by_name(track, "D8");
    TrainControlRequest request3 = {TRAINCONTROL_LOC, train1->id, track_node_id2, 0};
    send_delayed_track_command(&request3);

    int track_node_id3 = find_track_node_id_by_name(track, "D14");
    TrainControlRequest request4 = {TRAINCONTROL_MV, train1->id, 8, 50, track_node_id3}; //in 500 ms
    send_delayed_track_command(&request4);
    TrainControlRequest request5 = {TRAINCONTROL_MV, train2->id, 8, 50, track_node_id3}; //in 500 ms
    send_delayed_track_command(&request5);
  }

}

// Set all switches to Curved   (0x9a and 0x9c to Straight);
// 
// COM1 2400 baud rate: 4ms to transmit 1 byte, 8ms to transmit 1 switch command (2 bytes).
// Total time to run: 22 switches * 8ms = 176 ms.
// Deactivating solenoids should be delayed by at least 250ms.
void init_switches(int uart_tid, int* switches) {
  int i = 0, switch_position = SWITCH_CURVED;

  for(i=0; i<SWITCHES_ON_TRACK; i++) {
    if(i < SWITCHES_ON_TRACK-4 || i == switch_id_to_index(0x99) || i == switch_id_to_index(0x9b)) {
      switch_position = SWITCH_CURVED;
    } else if (i == switch_id_to_index(0x9a) || i == switch_id_to_index(0x9c)) {
      switch_position = SWITCH_STRAIGHT;
    }
    set_switch_position(uart_tid, switches, switch_index_to_id(i), switch_position, 0);
  }
  deactivate_solenoids_delayed(25); //in 250ms
}

int is_a_center_switch(int switch_id) {
  return (switch_id == 0x99 || switch_id == 0x9a || switch_id == 0x9b || switch_id == 0x9c) ? 1 : 0;
}

int get_opposite_center_switch(int switch_id) {
  int opposite_switch_id = 0;
  if      (switch_id == 0x99) opposite_switch_id = 0x9a;
  else if (switch_id == 0x9a) opposite_switch_id = 0x99;
  else if (switch_id == 0x9b) opposite_switch_id = 0x9c;
  else if (switch_id == 0x9c) opposite_switch_id = 0x9b;
  return opposite_switch_id;
}

// Switches can have two possible configurations: straight (S=33), and curved (C=34)
// Switch Ids are in ranges 1..18 and 0x99..0x9c (153..156).
void set_switch_position(int uart_tid, int* switches, int switch_id, int position, int set_solenoids_off) {
  if(!(1 <= switch_id && switch_id <= 18) && !(0x99 <= switch_id && switch_id <= 0x9c)) {
    switch_id = 1;
  }
  switches[switch_id_to_index(switch_id)] = position;
  print_switch(uart_tid, switches, switch_id_to_index(switch_id));

  Putc(uart_tid, 1, position);
  Putc(uart_tid, 1, switch_id);

  if (set_solenoids_off != 0) deactivate_solenoids_delayed(10); //in 100ms
}

// Each switch has a solenoid spiral that is activated on switching.
// Spiral MUST be deactivated, otherwise it will burn out (with 150ms-500ms delay after activating the switch).
// In case multiple switches are activated, the solenoid command needs to be sent ONLY once,
// as activating next switch deactivates the previous solenoid.
void deactivate_solenoids_delayed(int delay){ //in 10ms ticks
  TrainControlRequest request = {TRAINCONTROL_SOLENOID_OFF, 0, 0, delay};
  send_delayed_track_command(&request);
}

void deactivate_solenoids(int uart_tid){
  Putc(uart_tid, 1, SOLENOID_OFF);
}

int switch_index_to_id(int switch_index) {
  int switch_id = 0;
  if(switch_index >= 0 && switch_index <= 17) switch_id = switch_index + 1;
  else if (switch_index == 18) switch_id = 0x99;
  else if (switch_index == 19) switch_id = 0x9a;
  else if (switch_index == 20) switch_id = 0x9b;
  else if (switch_index == 21) switch_id = 0x9c;
  return switch_id;
}

int switch_index_to_branch_node_index(int switch_index) {
  return (switch_index == -1) ? switch_index : 80 + switch_index*2;
}

int switch_id_to_index(int switch_id) {
  int switch_index = 0;
  if(switch_id >= 1 && switch_id <= 18) switch_index = switch_id - 1;
  else if (switch_id == 0x99) switch_index = 18;
  else if (switch_id == 0x9a) switch_index = 19;
  else if (switch_id == 0x9b) switch_index = 20;
  else if (switch_id == 0x9c) switch_index = 21;
  return switch_index;
}

char switch_command_id_to_command_name(int command_id) {
  char command_name = 'C';
  if    (command_id == 33) command_name = 'S';
  else if (command_id == 34) command_name = 'C';
  return command_name;
}

void print_switches(int uart_tid, int* switches) {
  int i;
  for(i=0; i<SWITCHES_ON_TRACK; i++) {
    print_switch(uart_tid, switches, i);
  }
}

void print_switch(int uart_tid, int* switches, int switch_index) {
  int row = 4 + switch_index, column = 5;
  Printfrect(uart_tid, 2, row, column  , 0, PRINT_NOFILL, 
    "%u",
    switch_index_to_id(switch_index)
  );
  Printfrect(uart_tid, 2, row, column+6, 0, PRINT_NOFILL, 
    "%c",
    switch_command_id_to_command_name(switches[switch_index])
  );
}

// --- sensors ---

void init_sensors(int uart_tid) {
  Putc(uart_tid, 1, RESET_SENSORS_AFTER_DUMP);
}

track_node* get_sensor_node_from_data_byte(char* c, int sensors_bytes_received, track_node *track) {
  volatile int sensor_decoder_index=0, byte_id=0, sensor_index=-1, sensor_node_index=-1;
  track_node *sensor_node = 0;
  sensor_decoder_index = get_sensor_decoder_id_from_data_byte(sensors_bytes_received);
  byte_id = (sensors_bytes_received % 2 > 0) ? 1 : 2; // Each sensor returns 2 bytes
  sensor_index = get_sensor_index_from_data_byte(c, byte_id);
  sensor_node_index = get_sensor_node_index(sensor_decoder_index, sensor_index);
  if(sensor_node_index != -1) sensor_node = &track[sensor_node_index];
  return sensor_node; 
}

int get_sensor_node_index(int sensor_decoder_id, int sensor_index) {
  return sensor_index == -1 ? sensor_index : sensor_decoder_id*16 + sensor_index;
}

//zero-based: 0 to 15
int get_sensor_index_from_data_byte(char* c, int byte_id) {
    volatile int i = 1, triggered_sensor = -1;

    for(i; i<=8; i++) {
        if(is_sensor_bit_set(i, c)) {
            triggered_sensor = i-1 + ((byte_id == 2) ? 8 : 0);
            break;
        }
    }
    return triggered_sensor;
}

//bit_position: from left to right. Ex. 0100_0000: 2nd bit is set. 
int is_sensor_bit_set(int bit_position, char* c) {
    return *c & (128 >> (bit_position - 1));
}

int get_sensor_decoder_id_from_data_byte(int sensors_bytes_received) {
    int sensor_id = 0;
    if     (1 <= sensors_bytes_received && sensors_bytes_received <= 2) sensor_id = SENSOR_A;
    else if(3 <= sensors_bytes_received && sensors_bytes_received <= 4) sensor_id = SENSOR_B;
    else if(5 <= sensors_bytes_received && sensors_bytes_received <= 6) sensor_id = SENSOR_C;
    else if(7 <= sensors_bytes_received && sensors_bytes_received <= 8) sensor_id = SENSOR_D;
    else if(9 <= sensors_bytes_received && sensors_bytes_received <= 10) sensor_id = SENSOR_E;
    return sensor_id;
}

void set_edge_to_next_sensor_node(track_node* current_sensor_node, track_edge* edge, int switches[]) {
  track_node* next_node = 0;
  if(current_sensor_node) {
    edge->src = current_sensor_node;
    edge->dist = 0;
    int direction = DIR_AHEAD;
    while(1) {
      next_node = current_sensor_node->edge[direction].dest;
      edge->dist += current_sensor_node->edge[direction].dist;
      if(next_node->type == NODE_SENSOR || next_node->type == NODE_EXIT) {
        edge->dest = next_node;
        break;
      } else if (next_node->type == NODE_BRANCH) {
        direction = (switches[switch_id_to_index(next_node->num)] == SWITCH_STRAIGHT) ? DIR_STRAIGHT : DIR_CURVED;
      } else {
        direction = DIR_AHEAD;
      }
      current_sensor_node = next_node;
    }
  }
}

int track_edges_equal(track_edge* lhs, track_edge* rhs) {
  return (lhs->src == rhs->src && lhs->dest == rhs->dest) ? 1 : 0; 
}

void copy_track_edge(track_edge* lhs, track_edge* rhs) {
  lhs->reverse = rhs->reverse;
  lhs->src     = rhs->src;
  lhs->dest    = rhs->dest;
  lhs->dist    = rhs->dist;
}

int is_one_of_first_five_sensors_on_path(Train* train, track_node* sensor_node) {
  int sensors_on_path = 0;

  if(!sensor_node || !train->pathstart) return 0;
  int i = 0;
  for(i=0;i<TRACK_MAX;i++) {
    if(&(train->path[i]) == train->pathstart) break;
  }
  while(1) {
    if(i>=TRACK_MAX) break;
    if(train->path[i].node->type == NODE_SENSOR) {
      sensors_on_path++;
      if(train->path[i].node == sensor_node) {
        break;
      }
    }
    i++;
  }
  return (0 < sensors_on_path && sensors_on_path <= 5) ? 1 : 0;
}

Train* attribute_sensor_node_to_train(int clock_tid, Train trains[], track_node* current_sensor_node) {
  if(!current_sensor_node) return 0;
  int i = 0;
  Train* train = 0;
  // int time_now = Time(clock_tid) * 10; //in ms
  for(i=0; i<TOTAL_TRAINS; i++) {
    if((!trains[i].current_sensor_on_path &&
         ((current_sensor_node == trains[i].current_sensors_edge.dest) ||
          (trains[i].pathstart && is_one_of_first_five_sensors_on_path(&trains[i], current_sensor_node)))) ||

       (trains[i].next_sensor_on_path && current_sensor_node == trains[i].next_sensor_on_path->node) ||
           // && abs(time_now - trains[i].next_sensor_on_path_eta) < TRAIN_SENSOR_ATTRIB_TIMEOUT_MS) ||

       (trains[i].second_next_sensor_on_path && current_sensor_node == trains[i].second_next_sensor_on_path->node)) {
           // && abs(time_now - trains[i].second_next_sensor_on_path_eta) < TRAIN_SENSOR_ATTRIB_TIMEOUT_MS)) {

      train = &trains[i];
      break;
    }
  }
  return train;
}

void train_assign_sensor_passed(int uart_tid, int clock_tid, track_node* track, int switches[], 
    Train* train, track_node* sensor_node, Train *other_trains) {
  
  if(!sensor_node || !train) return;
  set_edge_to_next_sensor_node(sensor_node, &train->current_sensors_edge, switches);

  if(!track_edges_equal(&train->current_sensors_edge, &train->prev_sensors_edge)) {
    copy_track_edge(&train->prev_sensors_edge, &train->current_sensors_edge);
    train_adjust_velocity(uart_tid, clock_tid, train, track, switches);
    print_train_stats(uart_tid, train);
    train_adjust_path(uart_tid, clock_tid, train, sensor_node, track, switches, other_trains);
  }
}

void measure_velocity(int uart_tid, Train* train, track_node *current_sensor_node,
    int distance_measurements[], int* start_time, int* finish_time, int* distance_measurement_id,
    int start_time_measurements[], int* start_time_measurement_id, 
    int finish_time_measurements[], int* finish_time_measurement_id) {

  // int a3_e7_distance = 1352; //mm
  int train_id = 58;
  int delay = 85; // in clockserver ticks by 10ms
  int delay_ms = delay * 10; // in ms
  int speed = 14;

  int time_receive_sensor_report = 60 * 2; // sensors are hit two times
  int time_to_deliver_stop_command = 30; // in ms

  int time_now = read_system_timer(), total_time = 0;
  // if(current_sensor_node->num == 44) { // C13
  //   TrainControlRequest request = {TRAINCONTROL_TR, train_id, 0, 0};
  //   send_delayed_track_command(&request);
  // }
  if(current_sensor_node->num == 2) { //2 A3   //44) { // C13
    start_time_measurements[*start_time_measurement_id] = time_now;
    Printf(uart_tid, 2, "\033[%d;%dHA3 %d: %d",
        45+*start_time_measurement_id, FRAME_WIDTH+2, *start_time_measurement_id+1, time_now);
    if((*start_time_measurement_id) > 20) (*start_time_measurement_id) = 0;
    (*start_time_measurement_id)++;
    if(time_now - *start_time > 4000000) { // ==1 sec, guard from repeated sensor reading
      *start_time = time_now;

      TrainControlRequest request = {TRAINCONTROL_TR, train_id, 0, delay}; // 950ms
      send_delayed_track_command(&request);
    }
  } else if (current_sensor_node->num == 70) { // E7
      finish_time_measurements[*finish_time_measurement_id] = time_now;
      Printf(uart_tid, 2, "\033[%d;%dHE7 %d: %d",
          45+*finish_time_measurement_id, FRAME_WIDTH+18, *finish_time_measurement_id+1, time_now);
      if((*finish_time_measurement_id) > 20) (*finish_time_measurement_id) = 0;
      (*finish_time_measurement_id)++;
    if(time_now - *finish_time > 4000000) {
      *finish_time = time_now;

      total_time = (*finish_time - *start_time) * TIMER4_MS_RATIO;
      Printf(uart_tid, 2, "\033[%d;%dHA3 E7 time %d: %d",
          15+*distance_measurement_id, 30, *distance_measurement_id+1, total_time);

      //Distance travelled with const speed: S = V*t
      int distance_with_const_speed = (get_velocity_for_speed(train, speed) / 1000) * delay_ms;  // mm/ms * ms = mm
      Printf(uart_tid, 2, "\033[%d;%dHDist with const V=%u, S=%u",
          15+*distance_measurement_id, 55, (int) get_velocity_for_speed(train, speed), distance_with_const_speed);

      int time_decelerating = total_time - delay_ms;
      Printf(uart_tid, 2, "\033[%d;%dHTime decelerating t=%d", 15+*distance_measurement_id, 90, time_decelerating);

      //Distance decelerating to full stop with constant acceleration: S = V*t / 2
      int distance_decelerating = (get_velocity_for_speed(train, speed) / 1000) * time_decelerating / 2;
      Printf(uart_tid, 2, "\033[%d;%dHDist decelerating S=%d",
          15+*distance_measurement_id, 120, distance_decelerating);

      // Subtracting constant factors from decelerating distance
      int distance_stopping = distance_decelerating -
          (get_velocity_for_speed(train, speed) / 1000) * (time_receive_sensor_report + time_to_deliver_stop_command);
      Printf(uart_tid, 2, "\033[%d;%dHDist stopping Ds=%d", 15+*distance_measurement_id, 150, distance_stopping);

      volatile int distance_measurement_index = *distance_measurement_id;
      distance_measurements[distance_measurement_index] = time_decelerating;
      (*distance_measurement_id)++;

      TrainControlRequest request = {TRAINCONTROL_TR, train_id, speed, delay*3};
      send_delayed_track_command(&request);

      if(*distance_measurement_id == MEASUREMENTS_MAX) {
        volatile int i = 0, average_dist = 0;
        for(i=0; i<MEASUREMENTS_MAX; i++)
          average_dist += distance_measurements[i];
        average_dist = (int) average_dist / MEASUREMENTS_MAX;

        Printf(uart_tid, 2, "\033[%d;%dHAverage stopping time: %d",
            15+*distance_measurement_id, 30, distance_measurements[0]);
      }
    }
  }
}
