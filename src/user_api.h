#ifndef __K_API_H___
#define __K_API_H___

#include "user_system_tasks.h"

enum {
  EVENT_TIMER_TICK,
  EVENT_UART1_TRANSMIT,
  EVENT_UART2_TRANSMIT,
  EVENT_UART1_RECEIVE,
  EVENT_UART2_RECEIVE,
  EVENT_MAX
};

#define FOREVER for(;;)
#define ARRAY_SIZE(x)  (sizeof(x) / sizeof((x)[0]))
static inline int abs(int x) { return x >= 0 ? x : -x; }
static inline int max(int a, int b) { return a >= b ? a : b; }
static inline int min(int a, int b) { return a <= b ? a : b; }
typedef char *va_list;
#define __va_argsiz(t)  (((sizeof(t) + sizeof(int) - 1) / sizeof(int)) * sizeof(int))
#define va_start(ap, pN) ((ap) = ((va_list) __builtin_next_arg(pN)))
#define va_end(ap)  ((void)0)
#define va_arg(ap, t) (((ap) = (ap) + __va_argsiz(t)), *((t*) (void*) ((ap) - __va_argsiz(t))))

int Create(int priority, void (*code)());
int MyTid();
int MyParentTid();
void Pass();
void Exit();
int Destroy(int tid);
void AtExit(void (*handler)(void));
int Send(int tid, char *msg, int msglen, char *reply, int rplen);
int Receive(int *tid, char *msg, int msglen);
int Reply(int tid, char *reply, int rplen);

int RegisterAs(char *name);
int WhoIs(char *name);

int AwaitEvent(int eventid, char byte);
int Delay(int tid, int ticks);
int Time(int tid);
int DelayUntil(int tid, int ticks);

int Getc(int tid, int uart);
int Putc(int tid, int uart, char c);
int Putstr(int tid, int uart, char* s);
void Printf(int tid, int uart, const char *fmt, ...);

#define PRINT_NOSAVECURSOR 1
#define PRINT_NOFILL 2

void Printfrect(int tid, int uart, int row, int column, int width, int flags, const char *fmt, ...);

int RunningTime(int tid, int percentage);
int TaskCount();

inline int Random(int clock_tid, int min, int max);

#endif
