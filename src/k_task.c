#include "k_globals.h"
#include "k_task.h"
#include "k_taskdescriptor.h"
#include "k_scheduler.h"
#include "k_interrupts.h"
#include "k_message.h"
#include "user_api.h"
#include "ts7200.h"

#include "syscall_numbers.h"

#define VECTOR_BASE 0x20
#define VECTOR_SVCALL_OFFSET 0x8
#define VECTOR_IRQCALL_OFFSET 0x18

/**
CURRENT STACK FRAME LAYOUT
Val   psp offset
PC  | 0x38
r14 | 0x34
r12 | 0x30
r11 | 0x2c
... |
r1  | 0x04
r0  | 0x00

If this changes, offsets in this file must also be changed.
**/

int k_create_task(KGlobals* globals, int priority, void (*code)()) {
  TaskDescriptor *td;

  if (priority < 0 || priority >= NPRIORITIES) {
    return -1;
  }

  td = globals->task_next;
  if (td == 0 || globals->tid_next == TID_MAX) {
    return -2;
  }
  globals->task_next = td->next;

  td->tid = ((td - &globals->tasks[0]) << 16) + globals->tid_next++;
  td->parent = globals->running_task;
  td->priority = priority;
  td->sp = (int *)globals->stack_next->stack_top;
  td->stack = globals->stack_next;
  td->psr = 0x50; // user mode, interrupts on (0xd0 interrupts off)
  td->running_time = 0;
  td->waiting_senders = 0;
  
  td->child = 0;
  td->next_sibling = 0;
  add_child_task_pointer(globals, td->parent, td);
  td->at_exit = 0;

  *--(td->sp) = (int)code;// PC in stack frame
  td->sp-=14; // Reserve space for r14, r12 -> r0

  globals->stack_next = globals->stack_next->next;

  td->state = TS_READY;
  add_ready_task(globals, td);

  globals->task_count++;

  return td->tid;
}

void k_destroy_task(KGlobals* globals, int tid) {
  TaskDescriptor *td = k_lookup_tid(globals, tid);
  if(td) {
    if (td->state == TS_BLOCKED_EVENT) remove_task_event_queues(globals, td);
    else if (td->state == TS_BLOCKED_SEND) remove_task_send_queue(globals, td);
    clear_reply_blocked(globals, td);

    postorder_dfs_task_traversal(globals, td, k_exit_task);
    remove_child_task_pointer(globals, td->parent, td);
  }
}

void k_exit_task(KGlobals* globals, TaskDescriptor *td) {
  if (td->at_exit) td->at_exit();
  td->state = TS_MAX;
  remove_ready_task(globals, td);
  globals->task_count--;
  reclaim_task_resources(globals, td);
}

static void k_task_wait_event(KGlobals *globals, TaskDescriptor *td, int eventid, char byte) {
  td->next_event_waiting = globals->event_queues[eventid];
  globals->event_queues[eventid] = td;
  globals->event_blocked_mask |= (1 << eventid);

  td->state = TS_BLOCKED_EVENT;
  remove_ready_task(globals, td);

  if (eventid == EVENT_UART1_TRANSMIT) {
    volatile int *uart1_ctrl = (int *)(UART1_BASE + UART_CTLR_OFFSET);
    *uart1_ctrl |= TIEN_MASK;
    td->output_byte = byte;
  } else if (eventid == EVENT_UART2_TRANSMIT) {
    volatile int *vic1_enable = (int *)(VIC1_BASE + VIC_INT_ENABLE_OFFSET);
    *vic1_enable = UART2_TXIRQ_MASK;
    td->output_byte = byte;
  }
}

static void handle_syscall(KGlobals *globals, int syscall) {
  TaskDescriptor *td = globals->running_task;
  int *psp = td->sp;
  switch (syscall) {
    case SYSCALL_CREATE:
      k_task_set_return_int(td, k_create_task(globals, psp[0], (void (*)(void))psp[1]));
      break;

    case SYSCALL_MYTID:
      k_task_my_tid(globals);
      break;

    case SYSCALL_MYPARENTTID:
      k_task_my_parent_tid(globals);
      break;

    case SYSCALL_PASS:
      break;

    case SYSCALL_EXIT:
      k_exit_task(globals, td);
      break;

    case SYSCALL_DESTROY:
      k_destroy_task(globals, psp[0]);
      break;

    case SYSCALL_ATEXIT:
      td->at_exit = (void (*)(void))psp[0];
      break;

    case SYSCALL_SEND:
      k_send_message(globals, psp[0], (char *)psp[1], psp[2], (char *)psp[3], psp[14]);
      break;

    case SYSCALL_RECV:
      k_receive_message(globals, (int *)psp[0], (char *)psp[1], psp[2]);
      break;

    case SYSCALL_REPLY:
      k_reply_message(globals, psp[0], (char *)psp[1], psp[2]);
      break;

    case SYSCALL_AWAITEVENT:
      k_task_wait_event(globals, td, psp[0], psp[1]);
      break;

    case SYSCALL_RUNNINGTIME:
      k_task_running_time(globals, psp[0], psp[1]);
      break;

    case SYSCALL_TASKCOUNT:
      k_task_set_return_int(td, globals->task_count);
      break;
  }
}

void k_run_task(KGlobals* globals) {
  // Install SVC/SWI and IRQ Handlers
  *(int *)(VECTOR_BASE + VECTOR_SVCALL_OFFSET) = (int)&&svchandler;
  *(int *)(VECTOR_BASE + VECTOR_IRQCALL_OFFSET) = (int)&&irqhandler;

  int task_start_time = read_system_timer();

  TaskDescriptor *td = globals->running_task;
  volatile int *running_time = &td->running_time; /* needed to prevent CSE */
  register int *psp asm ("r0") = td->sp;
  register int psr asm ("r1") = td->psr;
  register int syscall asm ("r2");

  asm volatile (
    "STMDB SP!, {R4-R12, R14}\n"
    "LDR LR, [%[psp], #56]\n"
    "MSR SPSR, %[psr]\n"
    "MSR CPSR, #0xdf\n" // Enter System state (irq disabled)
    "MOV SP, %[psp]\n"
    "LDMIA SP!, {R0-R12, R14}\n"
    "ADD SP, SP, #4\n"
    "MSR CPSR, #0xd3\n" // re-enter SVC state (irq disabled)
    "MOVS PC, LR"
    :
    : [psp] "r" (psp), [psr] "r" (psr)
  );

svchandler:

  asm volatile (
    "MSR CPSR, #0xdf\n"// enter system state (irq disabled)
    "SUB SP, SP, #4\n"// reserve space for pc
    "STMDB SP!, {R0-R12, R14}\n"// save registers (irq disabled)
    "MOV %[psp], SP\n"// get PSP for saving to td
    "MSR CPSR, #0xd3\n"// re-enter SVC state
    "MRS %[psr], SPSR\n"// get SPSR for saving to td
    "STR lr, [%[psp], #56]\n"// Save program pc to stack
    "LDR %[syscall], [lr, #-4]\n"//Load SWI instruction
    "BIC %[syscall], %[syscall], #0xFF000000\n" 
    "LDMIA SP!, {R4-R12, R14}\n"//load saved registers
    : [psp] "=r" (psp), [psr] "=r" (psr), [syscall] "=r" (syscall)
    :
    : "r3"
  );

  td->psr = psr;
  td->sp = psp;
  *running_time += read_system_timer() - task_start_time;

  handle_syscall(globals, syscall);
  asm volatile ("b end");

irqhandler:

  asm volatile (
    "SUB lr, lr, #4\n"
    "MSR CPSR, #0xdf\n"// enter system state (irq disabled)
    "SUB SP, SP, #4\n"// reserve space for pc
    "STMDB SP!, {R0-R12, R14}\n"// save registers (irq disabled)
    "MOV %[psp], SP\n"// get PSP for saving to td
    "MSR CPSR, #0xd2\n"// re-enter IRQ state (irq disabled)
    "MRS %[psr], SPSR\n"// get SPSR for saving to td
    "STR lr, [%[psp], #56]\n"// Save program pc to stack
    "MSR CPSR, #0xd3\n"// re-enter SVC state (irq disabled)
    "LDMIA SP!, {R4-R12, R14}\n"//load saved kernel registers
    : [psp] "=r" (psp), [psr] "=r" (psr)
    :
    : "r3"
  );

  td->psr = psr;
  td->sp = psp;
  *running_time += read_system_timer() - task_start_time;

  handle_irq(globals);
  asm volatile ("end:");
}

TaskDescriptor *k_lookup_tid(KGlobals *globals, int tid) {
  TaskDescriptor *td = 0;
  int idx = tid_td_index(tid);
  if (idx >= 0 && idx < NTASKS && 
      globals->tasks[idx].state < TS_MAX && 
      globals->tasks[idx].tid == tid) {
    td = &globals->tasks[idx];
  }
  return td;
}

void k_task_my_tid(KGlobals* globals) {
  k_task_set_return_int(globals->running_task, globals->running_task->tid);
}

void k_task_my_parent_tid(KGlobals* globals) {
  TaskDescriptor *parent = globals->running_task->parent;
  k_task_set_return_int(globals->running_task, parent ? parent->tid : -1);
}

void k_task_running_time(KGlobals* globals, int tid, int percentage) {
  TaskDescriptor *target = k_lookup_tid(globals, tid);
  int running_time = -1;
  if(target) {
    running_time = target->running_time;
    if(percentage) running_time = ((float) target->running_time / read_system_timer()) * 10000;
  }
  k_task_set_return_int(globals->running_task, running_time);
}

// Stack frame manipulation helpers
void k_task_set_return_int(TaskDescriptor* td, int ret) {
  td->sp[0] = ret;
}
