#include "util_tid_queue.h"

void TidQueue_init(TidQueue* queue) {
  queue->head = 0;
  queue->tail = 0;
}

int TidQueue_hasElements(TidQueue* queue) {
  return queue->head != queue->tail;
}

void TidQueue_push(TidQueue* queue, int tid) {
  queue->elements[queue->tail] = tid;
  queue->tail++;
  if (queue->tail == NTASKS) {
    queue->tail = 0;
  }
}

int TidQueue_pop(TidQueue* queue) {
  int ret = queue->elements[queue->head];
  queue->head++;
  if (queue->head == NTASKS) {
    queue->head = 0;
  }
  return ret;
}
