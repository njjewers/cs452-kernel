#include "user_system_tasks.h"
#include "user_api.h"
#include "util_ringbuffer.h"
#include "util_tid_queue.h"
#include "k_globals.h"
#include "string.h"

#define COM1_RXBUFLEN 16
#define COM1_TXBUFLEN 256
#define COM2_RXBUFLEN 64
#define COM2_TXBUFLEN 2048

typedef struct {
  int rxnotifier;
  RingBuffer rx;
  TidQueue rxqueue;

  int txnotifier;
  int txnotifier_blocked;
  RingBuffer tx;
} Comport;

static void uart_rx_notifier(int uart, int rxevent) {
  int uart_service_tid = MyParentTid();

  UartServiceRequest request = { UARTSERVER_GETC, uart };

  for (;;) {
    int event = AwaitEvent(rxevent, 0);
    request.c = event;
    Send(uart_service_tid, (char*)&request, sizeof(request), (char*)0, 0);
  }
}

static void uart_tx_notifier(int uart, int txevent) {
  int uart_service_tid = MyParentTid();

  UartServiceRequest request = { UARTSERVER_PUTC, uart };
  char c;

  for (;;) {
    Send(uart_service_tid, (char*)&request, sizeof(request), &c, sizeof(c));
    AwaitEvent(txevent, c);
  }
}

static void com1_rxnotifier() {
  uart_rx_notifier(1, EVENT_UART1_RECEIVE);
}

static void com1_txnotifier() {
  uart_tx_notifier(1, EVENT_UART1_TRANSMIT);
}

static void com2_rxnotifier() {
  uart_rx_notifier(2, EVENT_UART2_RECEIVE);
}

static void com2_txnotifier() {
  uart_tx_notifier(2, EVENT_UART2_TRANSMIT);
}

void system_uart_server() {
  int tid; 
  UartServiceRequest request; 

  RegisterAs(UARTSERVICE_NAME);

  // Declare ringbuffer physical space
  char com1rx[COM1_RXBUFLEN];
  char com1tx[COM1_TXBUFLEN];
  char com2rx[COM2_RXBUFLEN];
  char com2tx[COM2_TXBUFLEN];

  Comport comports[2];

  // Buffer setup
  RingBuffer_init(&comports[0].rx, com1rx, COM1_RXBUFLEN);
  RingBuffer_init(&comports[0].tx, com1tx, COM1_TXBUFLEN);
  TidQueue_init(&comports[0].rxqueue);
  comports[0].txnotifier_blocked = 0;

  RingBuffer_init(&comports[1].rx, com2rx, COM2_RXBUFLEN);
  RingBuffer_init(&comports[1].tx, com2tx, COM2_TXBUFLEN);
  TidQueue_init(&comports[1].rxqueue);
  comports[1].txnotifier_blocked = 0;
 
  // Start notifiers
  comports[0].rxnotifier = Create(30, com1_rxnotifier);
  comports[0].txnotifier = Create(30, com1_txnotifier);
  
  comports[1].rxnotifier = Create(30, com2_rxnotifier);
  comports[1].txnotifier = Create(30, com2_txnotifier);

  for (;;) {
  receive:
    Receive(&tid, (char*)&request, sizeof(request));

    int i;
    for (i=0; i<2; i++) {
      Comport * port = &comports[i];
      if (port->rxnotifier == tid) {
        Reply(tid, (char*)0, 0);
        if (TidQueue_hasElements(&port->rxqueue)) {
          Reply(TidQueue_pop(&port->rxqueue), &request.c, sizeof(request.c));
        } else {
          RingBuffer_push(&port->rx, request.c);
        }

        goto receive;
      } else if (port->txnotifier == tid) {
        if (RingBuffer_hasElements(&port->tx)) {
          char c = RingBuffer_pop(&port->tx);
          Reply(port->txnotifier, &c, sizeof(c));
        } else {
          // No bytes to tx, block
          port->txnotifier_blocked = 1;
        }

        goto receive;// break nested loop
      }
    }

    Comport * port = &comports[request.uart-1];
    switch (request.type) {
      case UARTSERVER_GETC: {
        if (RingBuffer_hasElements(&port->rx)) {
          char c = RingBuffer_pop(&port->rx);
          Reply(tid, &c, sizeof(c));
        } else {
          // push to blocked queue
          TidQueue_push(&port->rxqueue, tid);
        }
      }break;
      case UARTSERVER_PUTC:{
        RingBuffer_push(&port->tx, request.c);
        Reply(tid, (char*)0, 0);
      }break;
      case UARTSERVER_PUTSTR:{
        RingBuffer_pushstr(&port->tx, request.s);
        Reply(tid, (char*)0, 0);
      }break;
    }

    if (port->txnotifier_blocked && RingBuffer_hasElements(&port->tx)) {
      // Now unblocked, send character to tx notifier
      port->txnotifier_blocked = 0;
      char c = RingBuffer_pop(&port->tx);
      Reply(port->txnotifier, &c, sizeof(c));
    }
  }
}
