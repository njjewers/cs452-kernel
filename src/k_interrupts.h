#ifndef __K_INTERRUPTS_H___
#define __K_INTERRUPTS_H___

#include "k_globals.h"

void set_timer_interrupt(int timer, int enable);
void clear_timer_interrupt(int timer);
void handle_irq(KGlobals *globals);
void disable_all_interrupts();

#endif
