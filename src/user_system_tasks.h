#ifndef __USER_SYSTEM_TASKS_H__
#define __USER_SYSTEM_TASKS_H__

#include "user_system_uart.h"

#define IDLETASK_NAME "idle"
#define CLOCKSERVICE_NAME "ClockService"
#define TRAINS_UI_SERVICE "TrainsUIService"
#define TRAINS_CONTROL_SERVICE "TrainsControlService"
#define TRAINS_USER_INPUT_SERVICE "TrainsUserInputService"

typedef enum {
  NAMESERVER_REGISTER,
  NAMESERVER_WHOIS
} NameServiceRequestType;

typedef struct {
  NameServiceRequestType type;
  char* name;
} NameServiceRequest;

void system_name_server();

typedef enum {
  CLOCKSERVER_TICK,
  CLOCKSERVER_QUERY,
  CLOCKSERVER_DELAY,
  CLOCKSERVER_DELAYUNTIL,
} ClockServiceRequestType;

typedef struct {
  ClockServiceRequestType type;
  int arg;
} ClockServiceRequest;

void system_clock_server();

void system_idle_task();

#endif
