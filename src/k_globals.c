#include "k_globals.h"
#include "user_api.h"

void KGlobals_init(KGlobals *globals, int kernel_stack_top) {
  int i, kernel_stack_end = kernel_stack_top - KERNEL_STACK_SIZE;

  globals->task_count = 0;

  // Task Descriptor freelist setup
  for (i=0; i<=NTASKS-1; i++) {
    globals->tasks[i].prev = (i != 0)        ? &globals->tasks[i-1] : 0;
    globals->tasks[i].next = (i != NTASKS-1) ? &globals->tasks[i+1] : 0;
    globals->tasks[i].state = TS_MAX;
  }

  globals->task_next = &globals->tasks[0];
  globals->task_last = &globals->tasks[NTASKS-1];

  globals->tid_next = 0;

  globals->running_task = 0;

  globals->ready_mask = 0;

  // Stack freelist setup
  for (i=0; i<=NTASKS-1; i++) {
    globals->stacks[i].prev = (i != 0)        ? &globals->stacks[i-1] : 0;
    globals->stacks[i].next = (i != NTASKS-1) ? &globals->stacks[i+1] : 0;
    globals->stacks[i].stack_top = kernel_stack_end - i * TASK_STACK_SIZE;
  }

  globals->stack_next = &globals->stacks[0];
  globals->stack_last = &globals->stacks[NTASKS-1];

  for (i = 0; i < EVENT_MAX; ++i)
    globals->event_queues[i] = 0;
  globals->event_blocked_mask = 0;

  globals->uart1_cts = 1;
  globals->uart1_waiting_cts = 0;
}
