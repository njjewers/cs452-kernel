#ifndef __K_TASK_H__
#define __K_TASK_H__

#include "k_globals.h"

int k_create_task(KGlobals* globals, int priority, void (*code)());
void k_run_task(KGlobals* globals);
void k_exit_task(KGlobals* globals, TaskDescriptor* td);

TaskDescriptor *k_lookup_tid(KGlobals *globals, int tid);

void k_task_my_tid(KGlobals* globals);
void k_task_my_parent_tid(KGlobals* globals);
void k_task_running_time(KGlobals* globals, int tid, int percentage);

void k_task_set_return_int(TaskDescriptor* td, int ret);

#endif
