#include "k_message.h"
#include "k_task.h"
#include "string.h"

void k_send_message(KGlobals *globals, int tid, char *msg, int msglen, char *reply, int rplen) {
  TaskDescriptor *sender = globals->running_task, *receiver = k_lookup_tid(globals, tid);
  if (!receiver) {
    k_task_set_return_int(sender, -2);
    return;
  }

  if (receiver->state == TS_BLOCKED_RECV) {
    if (receiver->message.msglen >= msglen) {
      memcpy(receiver->message.msg, msg, msglen);
      k_task_set_return_int(receiver, msglen);
    } else {
      k_task_set_return_int(receiver, -1);
    }
    *receiver->message.sender_tid = sender->tid;
    receiver->state = TS_READY;
    add_ready_task(globals, receiver);
    sender->state = TS_BLOCKED_REPLY;
  } else {
    sender->message.msg = msg;
    sender->message.msglen = msglen;
    sender->next_waiting_sender = receiver->waiting_senders;
    receiver->waiting_senders = sender;
    sender->state = TS_BLOCKED_SEND;
  }
  remove_ready_task(globals, sender);
  sender->message.reply = reply;
  sender->message.rplen = rplen;
  sender->message.recipient = receiver;
}

void k_receive_message(KGlobals *globals, int *tid, char *msg, int msglen) {
  TaskDescriptor *receiver = globals->running_task;
  if (receiver->waiting_senders) {
    TaskDescriptor *sender = receiver->waiting_senders;
    receiver->waiting_senders = sender->next_waiting_sender;
    sender->next_waiting_sender = 0;
    if (msglen >= sender->message.msglen) {
      memcpy(msg, sender->message.msg, sender->message.msglen);
      k_task_set_return_int(receiver, sender->message.msglen);
    } else {
      k_task_set_return_int(receiver, -1);
    }
    *tid = sender->tid;
    /* sender->state should be TS_BLOCKED_SEND */
    sender->state = TS_BLOCKED_REPLY;
  } else {
    receiver->message.msg = msg;
    receiver->message.msglen = msglen;
    receiver->message.sender_tid = tid;
    receiver->state = TS_BLOCKED_RECV;
    remove_ready_task(globals, receiver);
  }
}

void k_reply_message(KGlobals *globals, int tid, char *reply, int rplen) {
  TaskDescriptor *receiver = globals->running_task, *sender = k_lookup_tid(globals, tid);
  if (!sender) {
    k_task_set_return_int(receiver, -2);
    return;
  }

  if (sender->state != TS_BLOCKED_REPLY) {
    k_task_set_return_int(receiver, -3);
    return;
  }
  
  if (sender->message.rplen >= rplen) {
    memcpy(sender->message.reply, reply, rplen);
    k_task_set_return_int(sender, rplen);
    k_task_set_return_int(receiver, rplen);
  } else {
    k_task_set_return_int(sender, -1);
    k_task_set_return_int(receiver, -1);
  }
  sender->state = TS_READY;
  add_ready_task(globals, sender);
}

void remove_task_send_queue(KGlobals *globals, TaskDescriptor *td) {
  TaskDescriptor *receiver = td->message.recipient;
  if (receiver->waiting_senders == td) {
    receiver->waiting_senders = td->next_waiting_sender;
    return;
  }

  TaskDescriptor *waiting = receiver->waiting_senders;
  while (waiting->next_waiting_sender != td) waiting = waiting->next_waiting_sender;
  waiting->next_waiting_sender = td->next_waiting_sender;
}

void clear_reply_blocked(KGlobals *globals, TaskDescriptor *td) {
  int i;
  for (i = 0; i < globals->task_count; ++i) {
    TaskDescriptor *other_td = &globals->tasks[i];
    if (other_td->state == TS_BLOCKED_REPLY &&
        other_td->message.recipient == td) {
      k_task_set_return_int(other_td, -2);
      other_td->state = TS_READY;
      add_ready_task(globals, other_td);
    }
  }
}
