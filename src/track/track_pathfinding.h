#ifndef __TRACK_PATHFINDING__
#define __TRACK_PATHFINDING__
#include "track_data.h" //TRACK_MAX definition
#include "track_node.h"


typedef struct {
  track_node *node;
  int direction;
  int distance_to_end;

  int distance_to_next_branch;
  track_node* next_branch;

  int distance_to_next_sensor;
  track_node* next_sensor;
} TrackPath;

TrackPath *track_pathfind(TrackPath path[TRACK_MAX], track_node *track, track_node *start, track_node *end);
int find_track_node_id_by_name(track_node *track, const char* node_name);
int pick_random_exit_id(int clock_tid, int current_exit_id);
inline int get_exit_node_index_from_id(int random_exit_id);
inline int get_exit_id_from_node_index(int current_exit_index);

#endif
