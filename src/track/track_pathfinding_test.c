#include "track_pathfinding.h"
#include "track_data.h"
#include <stdio.h>
#include <assert.h>

int main() {
  track_node track[TRACK_MAX];
  init_trackb(track);

  TrackPath path[TRACK_MAX];

  TrackPath *pathstart;

  pathstart = track_pathfind(path, track, &track[0], &track[103]);

  assert(pathstart[0].node->type == NODE_SENSOR);
  assert(pathstart[0].node->num == 0);
  assert(pathstart[0].distance_to_end == 218);
  assert(pathstart[0].distance_to_next_branch == -1);
  assert(pathstart[1].node->type == NODE_MERGE);
  assert(pathstart[1].node->num == 12);
  assert(pathstart[1].distance_to_end == 0);
  assert(pathstart[1].distance_to_next_branch == -1);

  // 0-> 103 -> 101 -> 44 -> 70 -> 54 -> 97 -> 94 ->73 
  pathstart = track_pathfind(path, track, &track[0], &track[73]);

  assert(pathstart[0].node == &track[0]);
  assert(pathstart[0].distance_to_end == 2326);
  assert(pathstart[0].distance_to_next_branch == 2111);
  assert(pathstart[0].next_branch == &track[94]);
  assert(pathstart[0].distance_to_next_sensor == 468);
  assert(pathstart[0].next_sensor == &track[44]);

  assert(pathstart[1].node == &track[103]);
  assert(pathstart[1].distance_to_end == 2108);
  assert(pathstart[1].distance_to_next_branch == 1893);
  assert(pathstart[1].next_branch == &track[94]);
  assert(pathstart[1].distance_to_next_sensor == 250);
  assert(pathstart[1].next_sensor == &track[44]);

  assert(pathstart[2].node == &track[101]);
  assert(pathstart[2].distance_to_end == 1918);
  assert(pathstart[2].distance_to_next_branch == 1703);
  assert(pathstart[2].next_branch == &track[94]);
  assert(pathstart[2].distance_to_next_sensor == 60);
  assert(pathstart[2].next_sensor == &track[44]);

  assert(pathstart[3].node == &track[44]);
  assert(pathstart[3].distance_to_end == 1858);
  assert(pathstart[3].distance_to_next_branch == 1643);
  assert(pathstart[3].next_branch == &track[94]);
  assert(pathstart[3].distance_to_next_sensor == 785);
  assert(pathstart[3].next_sensor == &track[70]);

  assert(pathstart[4].node == &track[70]);
  assert(pathstart[4].distance_to_end == 1073);
  assert(pathstart[4].distance_to_next_branch == 858);
  assert(pathstart[4].next_branch == &track[94]);
  assert(pathstart[4].distance_to_next_sensor == 375);
  assert(pathstart[4].next_sensor == &track[54]);

  assert(pathstart[5].node == &track[54]);
  assert(pathstart[5].distance_to_end == 698);
  assert(pathstart[5].distance_to_next_branch == 483);
  assert(pathstart[5].next_branch == &track[94]);
  assert(pathstart[5].distance_to_next_sensor == 698);
  assert(pathstart[5].next_sensor == &track[73]);

  assert(pathstart[6].node == &track[97]);
  assert(pathstart[6].distance_to_end == 408);
  assert(pathstart[6].distance_to_next_branch == 193);
  assert(pathstart[6].next_branch == &track[94]);
  assert(pathstart[6].distance_to_next_sensor == 408);
  assert(pathstart[6].next_sensor == &track[73]);

  assert(pathstart[7].node == &track[94]);
  assert(pathstart[7].distance_to_end == 215);
  assert(pathstart[7].distance_to_next_branch == -1);
  assert(pathstart[7].next_branch == 0);
  assert(pathstart[7].distance_to_next_sensor == 215);
  assert(pathstart[7].next_sensor == &track[73]);
  assert(pathstart[7].direction == 1);

  assert(pathstart[8].node == &track[73]);
  assert(pathstart[8].distance_to_end == 0);
  assert(pathstart[8].distance_to_next_branch == -1);
  assert(pathstart[8].next_branch == 0);
  assert(pathstart[8].distance_to_next_sensor == -1);
  assert(pathstart[8].next_sensor == 0);
}
