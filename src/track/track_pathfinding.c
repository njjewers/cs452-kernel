#include "track_pathfinding.h"
#include "track_data.h" //TRACK_MAX definition
#include "track_node.h"
#include "string.h"
#include "user_api.h"

#ifndef DIST_INFINITY
#define DIST_INFINITY 0x7fffffff
#endif

typedef struct {
  int distance;
  track_node *node;
} TrackNodeHeapEntry;

typedef struct {
  int count;
  TrackNodeHeapEntry entries[TRACK_MAX];
} TrackNodeHeap;

/*
 * Specialized heap init: Initializes all nodes at int_max distance,
 * except start, which is initialized at 0.
 */

 // 144, 139 a, b respectively
void TrackNodeHeap_init(TrackNodeHeap * heap, track_node *track, track_node* start) {
  heap->count = TRACK_MAX;
  int i;
  int entry_index = 1;
  heap->entries[0].distance = 0;
  heap->entries[0].node = start;
  for (i=0; i<TRACK_MAX; i++) {
    track_node* node = &track[i];
    if (node == start) {
      continue;
    }
    heap->entries[entry_index].distance = DIST_INFINITY;
    heap->entries[entry_index].node = node;
    entry_index++;
  }
}

void TrackNodeHeap_push(TrackNodeHeap * heap, int distance, track_node* node) {
  int i = (heap->count)++;

  while (i>0) {
    int parent = (i-1) >> 1;
    if (heap->entries[parent].distance > distance) {
      // This would invalidate heap property, swap w/parent
      heap->entries[i] = heap->entries[parent];
    } else {
      break;
    }
    i = parent;
  }
  heap->entries[i].distance = distance;
  heap->entries[i].node = node;
}


void TrackNodeHeap_decreasePriority(TrackNodeHeap *heap, track_node* node, int distance) {
  int i;
  for (i=0; i<heap->count; i++) {
    if (heap->entries[i].node == node)
      break;
  }
  if (i==heap->count) {
    return;
  }

  TrackNodeHeapEntry entry;
  entry.node = node;
  entry.distance = distance;

  while (i>0) {
    int parent = (i-1) >> 1;
    if (heap->entries[parent].distance > distance) {
      // This would invalidate heap property, swap w/parent
      heap->entries[i] = heap->entries[parent];
    } else {
      break;
    }
    i = parent;
  }
  heap->entries[i] = entry;
  
}

int TrackNodeHeap_hasElements(TrackNodeHeap* heap) {
  return (heap->count==0) ? 0 : 1;
}

track_node *TrackNodeHeap_pop(TrackNodeHeap* heap) {
  if (heap->count == 0) return (track_node*)0;

  track_node* ret = heap->entries[0].node;

  int tail = --(heap->count);
  if (tail == 0) {
    return ret; // Heap now empty, fast return
  }

  int distance = heap->entries[tail].distance;

  int i = 0;
  int child = 1;
  while (child < heap->count) {
    if ((child+1 < heap->count) && (heap->entries[child+1].distance < heap->entries[child].distance)) {
      child++;
    }
    if (heap->entries[child].distance < distance) {
      // Would invalidate heap property
      heap->entries[i] = heap->entries[child];
    } else {
      // Would not invalidate heap property, break
      break;
    }
    i = child;
    child = (i<<1) + 1;
  }

  heap->entries[i].distance = distance;
  heap->entries[i].node = heap->entries[tail].node;

  return ret;
}

int track_node_get_edge_count(track_node* node) {
  const int const type_edge_count[] = {0, 1, 2, 1, 1, 0};
  return type_edge_count[node->type];
}

TrackPath *track_pathfind(TrackPath path[TRACK_MAX], track_node *track, track_node *start, track_node *end) {
  int i;
  int dist[TRACK_MAX];
  for (i=0; i<TRACK_MAX; i++) {
    dist[i] = DIST_INFINITY;
  }

  track_node *prev[TRACK_MAX];
  for (i=0; i<TRACK_MAX; i++) {
    prev[i] = (track_node*)0;
  }

  TrackNodeHeap heap;
  TrackNodeHeap_init(&heap, track, start);

  dist[start - track] = 0;

  while (TrackNodeHeap_hasElements(&heap)) {
    track_node* node = TrackNodeHeap_pop(&heap);

    if (dist[node - track] == DIST_INFINITY) {
      break;
    }

    if (node==end) {
      // Backtrack + return
      int index = TRACK_MAX-1;
      int total_distance = dist[node - track];

      int next_branch_distance = -1;
      track_node* next_branch = 0;
      int next_sensor_distance = -1;
      track_node* next_sensor = 0;

      track_node* nextnode = node;
      for (;;) {
          int node_distance = dist[node - track];

          path[index].node = node;
          path[index].distance_to_end = total_distance - node_distance;
          path[index].direction = DIR_AHEAD;
          path[index].next_branch = next_branch;
          path[index].next_sensor = next_sensor;


          if (next_branch_distance >= 0) {
            path[index].distance_to_next_branch = next_branch_distance - node_distance;
          } else {
            path[index].distance_to_next_branch = -1;
          }

          if (node->type == NODE_BRANCH) {
            // branch direction determination
            for (i=0; i<2; i++) {
              if (node->edge[i].dest == nextnode) {
                path[index].direction = i;
                break;
              }
            }
            next_branch = node;
            next_branch_distance = node_distance;
          }

          if (next_sensor_distance >= 0) {
            path[index].distance_to_next_sensor = next_sensor_distance - node_distance;
          } else {
            path[index].distance_to_next_sensor = -1;
          }

          if (node->type == NODE_SENSOR) {
            next_sensor = node;
            next_sensor_distance = node_distance;
          } 

          if (node==start) {
            break;
          }

          nextnode = node;
          node = prev[node - track];
          index--;
      }

      return &path[index];
    }
  
    int edge_count = track_node_get_edge_count(node);
    for (i=0; i<edge_count; i++) {
      int alt = dist[node - track] + node->edge[i].dist;
      int index = node->edge[i].dest - track;
      if (alt < dist[index]) {
        dist[index] = alt;
        prev[index] = node;
        TrackNodeHeap_decreasePriority(&heap, node->edge[i].dest, alt);
      }
    }
  }
  
  return 0;
}

int find_track_node_id_by_name(track_node *track, const char* node_name) {
  int track_node_index = -1, i=0;
  for(i=0;i<TRACK_MAX;i++) {
    if(strcmp(node_name, track[i].name) == 0) {
      track_node_index = i;
      break;
    }
  }
  return track_node_index;
}

// pick exit 2 to 9. skip 0 and 1, as these are the exits in the inner loops
int pick_random_exit_id(int clock_tid, int current_exit_id) {
  int random_exit_id = -1;

  if(3 <= current_exit_id && current_exit_id <= 5) {
    random_exit_id = Random(clock_tid, 6, 9);
  } else {
    random_exit_id = Random(clock_tid, 3, 5);
  }

  // if picked EX8, go to EX7 instead, since switch 1 doesn't work on track A
  if(random_exit_id == 7) random_exit_id = 6;
  return random_exit_id;
}

inline int get_exit_node_index_from_id(int random_exit_id) {
  return 125 + random_exit_id*2;
}

inline int get_exit_id_from_node_index(int current_exit_index) {
  return (current_exit_index - 125) / 2;
}
