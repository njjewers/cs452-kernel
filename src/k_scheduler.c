#include "k_scheduler.h"

void add_ready_task(KGlobals *globals, TaskDescriptor *td) {
  int p = td->priority;
  td->next_ready = 0;
  td->prev_ready = 0;
  if (globals->ready_mask & (1 << p)) {
    globals->taskQueue[p].tail->next_ready = td;
    td->prev_ready = globals->taskQueue[p].tail;
  } else {
    globals->ready_mask |= (1 << p);
    globals->taskQueue[p].head = td;
  }
  globals->taskQueue[p].tail = td;
}

static inline int highest_set_bit(unsigned int n) {
  int r = 0;
  if (n & 0xFFFF0000) { n >>= 16; r |= 16; }
  if (n & 0xFF00) { n >>= 8; r |= 8; }
  if (n & 0xF0) { n >>= 4; r |= 4; }
  if (n & 0xC) { n >>= 2; r |= 2; }
  if (n & 0x2) { n >>= 1; r |= 1; }
  return r;
}

TaskDescriptor *get_next_ready_task(KGlobals *globals) {
  TaskDescriptor *td = 0;
  if (globals->ready_mask) {
    int p = highest_set_bit(globals->ready_mask);
    td = globals->taskQueue[p].head;
    if (td->next_ready) {
      // 1. Remove current head element from the list
      globals->taskQueue[p].head = td->next_ready;
      globals->taskQueue[p].head->prev_ready = 0;
      // 2. Push current head element to the tail
      globals->taskQueue[p].tail->next_ready = td;
      td->prev_ready = globals->taskQueue[p].tail;
      globals->taskQueue[p].tail = td;
      td->next_ready = 0;
    }
  }
  return td;
}

void remove_ready_task(KGlobals *globals, TaskDescriptor *td) {
  int p = td->priority;
  if (globals->ready_mask & (1 << p)) {
    TaskDescriptor* tdIterator = globals->taskQueue[p].tail;
    while(tdIterator) {
      if(tdIterator == td) {
        if(tdIterator == globals->taskQueue[p].head && tdIterator == globals->taskQueue[p].tail) {
          globals->ready_mask ^= (1 << p);
        } else if (tdIterator == globals->taskQueue[p].head) {
          globals->taskQueue[p].head = globals->taskQueue[p].head->next_ready;
          globals->taskQueue[p].head->prev_ready = 0;
        } else if (tdIterator == globals->taskQueue[p].tail) {
          globals->taskQueue[p].tail = globals->taskQueue[p].tail->prev_ready;
          globals->taskQueue[p].tail->next_ready = 0;
        } else {
          td->prev_ready->next_ready = td->next_ready;
          td->next_ready->prev_ready = td->prev_ready;
        }
        break;
      }
      tdIterator = tdIterator->prev_ready;
    }
  }
}

// Two pointers are involved in maintaining a tree of tasks: td->child and td->next_sibling.
// parent_td saves a pointer to this child_td as follows:
// 1. if child_td is the first child of parent_td, then parent_td->child should point to child_td;
// 2. if child_td is not the first child of parent_td, then one of parent_td's children->next_sibling should point to child_td.
//    This way, children list becomes traversable by getting to first child with parent->child, 
//    and then iterating over children with child->next_sibling.
void add_child_task_pointer(KGlobals *globals, TaskDescriptor* parent_td, TaskDescriptor* child_td) {
  if(!parent_td) return; // this is the first task ever, has no parent
  if(!parent_td->child){
    parent_td->child = child_td;
  } else {
    TaskDescriptor *sibling_td = parent_td->child;
    while(sibling_td){
      if(!sibling_td->next_sibling) {
        sibling_td->next_sibling = child_td;
        break;
      }
      sibling_td = sibling_td->next_sibling;
    }
  } 
}

// parent_td can have a pointer to this child_td in a couple of places:
// 1. if child_td is the first child of parent_td, then parent_td->child should point to child_td;
// 2. if child_td is not the first child of parent_td, then one of parent_td's children->next_sibling should point to child_td.
void remove_child_task_pointer(KGlobals *globals, TaskDescriptor* parent_td, TaskDescriptor* child_td) {
  if(parent_td->child == child_td){
    parent_td->child = child_td->next_sibling;
  } else {
    TaskDescriptor *sibling_td = parent_td->child;
    while(sibling_td){
      if(sibling_td->next_sibling == child_td) {
        sibling_td->next_sibling = child_td->next_sibling;
        break;
      }
      sibling_td = sibling_td->next_sibling;
    }
  } 
}

static void reclaim_td(KGlobals *globals, TaskDescriptor* td) {
  if(td->prev != 0) td->prev->next = td->next;
  if(td->next != 0) td->next->prev = td->prev;

  globals->task_last->next = td;
  td->prev = globals->task_last;
  td->next = 0;
  globals->task_last = td;
  if(globals->task_next == 0) globals->task_next = td;
}

static void reclaim_stack(KGlobals *globals, KStack* stack) {
  if(stack->prev != 0) stack->prev->next = stack->next;
  if(stack->next != 0) stack->next->prev = stack->prev;

  globals->stack_last->next = stack;
  stack->prev = globals->stack_last;
  stack->next = 0;
  globals->stack_last = stack;
  if(globals->stack_next == 0) globals->stack_next = stack;
}

void reclaim_task_resources(KGlobals *globals, TaskDescriptor* td) {
  reclaim_td(globals, td);
  reclaim_stack(globals, td->stack);
}

void remove_task_event_queues(KGlobals *globals, TaskDescriptor *td) {
  int i;
  for (i = 0; i < EVENT_MAX; ++i) {
    if (globals->event_queues[i] == td) {
      globals->event_queues[i] = td->next_event_waiting;
      if (!globals->event_queues[i]) 
        globals->event_blocked_mask &= ~(1 << i);
      return;
    }

    TaskDescriptor *waiting = globals->event_queues[i];
    while (waiting) {
      if (waiting->next_event_waiting == td) {
        waiting->next_event_waiting = td->next_event_waiting;
        return;
      } 

      waiting = waiting->next_event_waiting;
    }
  }
}

void postorder_dfs_task_traversal(
    KGlobals *globals, TaskDescriptor* td, void (*task_action)(KGlobals*, TaskDescriptor*)) {
  if(!td) return;
  if(td->child) {
    TaskDescriptor* next_child = td->child;
    while(next_child){
      postorder_dfs_task_traversal(globals, next_child, task_action);
      next_child = next_child->next_sibling;
    }
  }
  (*task_action)(globals, td);
}
