#include "config.h"
#include "user_api.h"
#include "syscall_numbers.h"
#include "user_system_tasks.h"
#include "string.h"

#define SVC(code) asm volatile ("SWI %0" : : "I" (code))
#define RETURN_R0 register int ret asm("r0");\
                  return ret;

int Create(int priority, void (*code)()) {
  SVC(SYSCALL_CREATE);
  RETURN_R0;
}

int MyTid() {
  SVC(SYSCALL_MYTID);
  RETURN_R0;
}

int MyParentTid() {
  SVC(SYSCALL_MYPARENTTID);
  RETURN_R0;
}
 
void Pass() {
  SVC(SYSCALL_PASS);
}

void Exit() {
  SVC(SYSCALL_EXIT);
}

int Destroy(int tid) {
  SVC(SYSCALL_DESTROY);
  RETURN_R0;
}

void AtExit(void (*handler)(void)) {
  SVC(SYSCALL_ATEXIT);
}

int Send(int tid, char *msg, int msglen, char *reply, int rplen) {
  SVC(SYSCALL_SEND);
  RETURN_R0;
}

int Receive(int *tid, char *msg, int msglen) {
  SVC(SYSCALL_RECV);
  RETURN_R0;
}

int Reply(int tid, char *reply, int rplen) {
  SVC(SYSCALL_REPLY);
  RETURN_R0;
}

int AwaitEvent(int eventid, char byte) {
  SVC(SYSCALL_AWAITEVENT);
  RETURN_R0;
}

// Nameserver API

int RegisterAs(char *name) {
  NameServiceRequest ns_request = {NAMESERVER_REGISTER, name};

  int status = Send(NAME_SERVER_TID, (char *) &ns_request, sizeof(ns_request), (char*)0, 0);
  return status < 0 ? -1 : 0; 
}

int WhoIs(char *name) {
  int tid;
  NameServiceRequest ns_request = {NAMESERVER_WHOIS, name};
  
  int status = Send(NAME_SERVER_TID, (char *) &ns_request, sizeof(ns_request), (char *) &tid, sizeof(tid));
  return status < 0 ? -1 : tid; 
}

// Clock server API

int Delay(int tid, int ticks) {
  if (ticks<=0)
    return -2;

  ClockServiceRequest request = {CLOCKSERVER_DELAY, ticks};
  int status = Send(tid, (char*)&request, sizeof(request), (char*)0, 0);
  return status == 0 ? 0 : -1;
}

int Time(int tid) {
  ClockServiceRequest request = {CLOCKSERVER_QUERY, 0};

  int tickCount;
  int status = Send(tid, (char*)&request, sizeof(request), (char*)&tickCount, sizeof(tickCount));

  return status < 0 ? -1 : tickCount;
}

int DelayUntil(int tid, int ticks) {
  if (ticks<=0)
    return -2;

  ClockServiceRequest request = {CLOCKSERVER_DELAYUNTIL, ticks};
  int status = Send(tid, (char*)&request, sizeof(request), (char*)0, 0);
  return status == 0 ? 0 : -1;
}

// UART server API

int Getc(int tid, int uart) {
  char retval;
  UartServiceRequest request = {UARTSERVER_GETC, uart};
  int status = Send(tid, (char*)&request, sizeof(request), (char*)&retval, sizeof(retval));
  return status < 0 ? -1 : retval;
}

int Putc(int tid, int uart, char c) {
  int retval;
  UartServiceRequest request = {UARTSERVER_PUTC, uart};
  request.c = c;
  int status = Send(tid, (char*)&request, sizeof(request), (char*)&retval, sizeof(retval));
  return status < 0 ? -1 : retval;
}

int Putstr(int tid, int uart, char* s) {
  int retval;
  UartServiceRequest request = {UARTSERVER_PUTSTR, uart};
  request.s = s;
  int status = Send(tid, (char*)&request, sizeof(request), (char*)&retval, sizeof(retval));
  return status < 0 ? -1 : retval;
}

char * strappend(char * dest, char * src) {
  int i = 0;
  // limit to 5000 chars, in case a string doesn't have null-termination
  while (*src && i<5000) {
    *dest++ = *src++;
    i++;
  }
  return dest;
};

static void format_time(char * buf, int time) {
    time/=10;
    buf[10] = '\0';
    buf[9] = '0' + time%10;
    buf[8] = ':';
    time/=10;
    buf[7] = '0' + time%10;
    buf[6] = '0' + (time%60)/10;
    buf[5] = ':';
    time/=60;
    buf[4] = '0' + time%10;
    buf[3] = '0' + (time%60)/10;
    buf[2] = ':';
    time/=60;
    buf[1] = '0' + time%10;
    buf[0] = '0' + (time/10)%10;  
}

static void format(int tid, int uart, const char *fmt, va_list va) {
  char outbuf[1024];
  char tmp[12];
  char *buf = outbuf;
  char ch;
  while ((ch = *fmt++)) {
    if (ch != '%') {
      *buf++ = ch;
    } else {
      ch = *fmt++;
      switch (ch) {
        case 0: 
          return;
        case 'c':
          *buf++ = va_arg(va, char);
          break;
        case 's':
           buf = strappend(buf, va_arg(va, char *));
          break;
        case 'u':
          uitoa(va_arg(va, int), 10, tmp);
          buf = strappend(buf, tmp);
          break;
        case 'd':
          itoa(va_arg(va, int), 10, tmp);
          buf = strappend(buf, tmp);
          break;
        case 'x':
          uitoa(va_arg(va, int), 16, tmp);
          buf = strappend(buf, tmp);
          break;
        case 't':
          format_time(tmp, va_arg(va, int));
          buf = strappend(buf, tmp);
          break;
        case '%':
          *buf++ = '%';
          break;
      }
    }
  }
  *buf = '\0';

  Putstr(tid, uart, outbuf);
}

void Printf(int tid, int uart, const char *fmt, ...) {
  va_list va;
  va_start(va, fmt);
  format(tid, uart, fmt, va);
  va_end(va);
}

static void formatrect(int tid, int uart, int row, int column, int width, int flags, const char *fmt, va_list va) {
  char outbuf[1024];
  char tmp[12];
  char *linestart;
  char *buf = outbuf;
  char ch;
  int i;

  if (!(flags & PRINT_NOSAVECURSOR)) {
    *buf++ = '\033';
    *buf++ = '[';
    *buf++ = 's';
  }

  *buf++ = '\033';
  *buf++ = '[';
  itoa(row++, 10, tmp);
  buf = strappend(buf, tmp);
  *buf++ = ';';
  itoa(column, 10, tmp);
  buf = strappend(buf, tmp);
  *buf++ = 'H';
  linestart = buf;

  while ((ch = *fmt++)) {
    if (ch == '\n') {
      if (!(flags & PRINT_NOFILL)) {
        for (i=(buf-linestart); i<width; i++) {
          *buf++ = ' ';
        }
      }

      *buf++ = '\033';
      *buf++ = '[';
      itoa(row++, 10, tmp);
      buf = strappend(buf, tmp);
      *buf++ = ';';
      itoa(column, 10, tmp);
      buf = strappend(buf, tmp);
      *buf++ = 'H';

      linestart = buf;
    } else if (ch != '%') {
      *buf++ = ch;
    } else {
      ch = *fmt++;
      switch (ch) {
        case 0: 
          return;
        case 'c':
          *buf++ = va_arg(va, char);
          break;
        case 's':
           buf = strappend(buf, va_arg(va, char *));
          break;
        case 'u':
          uitoa(va_arg(va, int), 10, tmp);
          buf = strappend(buf, tmp);
          break;
        case 'd':
          itoa(va_arg(va, int), 10, tmp);
          buf = strappend(buf, tmp);
          break;
        case 'x':
          uitoa(va_arg(va, int), 16, tmp);
          buf = strappend(buf, tmp);
          break;
        case 't':
          format_time(tmp, va_arg(va, int));
          buf = strappend(buf, tmp);
          break;
        case '%':
          *buf++ = '%';
          break;
      }
    }
  }
  if (!(flags & PRINT_NOFILL)) {
    for (i=(buf-linestart); i<width; i++) {
      *buf++ = ' ';
    }
  }

  if (!(flags & PRINT_NOSAVECURSOR)) {
    *buf++ = '\033';
    *buf++ = '[';
    *buf++ = 'u';
  }

  *buf = '\0';

  Putstr(tid, uart, outbuf);
}

void Printfrect(int tid, int uart, int row, int column, int width, int flags, const char *fmt, ...) {
  va_list va;
  va_start(va, fmt);
  formatrect(tid, uart, row, column, width, flags, fmt, va);
  va_end(va);
}

int RunningTime(int tid, int percentage) {
  SVC(SYSCALL_RUNNINGTIME);
  RETURN_R0;
}

int TaskCount() {
  SVC(SYSCALL_TASKCOUNT);
  RETURN_R0;
}

//min and max included
inline int Random(int clock_tid, int min, int max) {
  return min + Time(clock_tid) % (max+1 - min);
}
