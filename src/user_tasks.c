#include "config.h"
#include "user_api.h"
#include "string.h"
#include "user_system_tasks.h"
#include "train_control_server.h"
#include "trains_ui.h"
#include "trains_user_input.h"

void first_user_task() {
  Create(20, system_name_server);
  Create(20, system_clock_server);
  Create( 0, system_idle_task);
  Create(20, system_uart_server);

  Create(10, trains_ui_server);
  Create(10, train_control_server);
  Create(10, trains_user_input_server);
  Exit();
}

void quit_all_tasks() {
  int idle_task_tid = WhoIs(IDLETASK_NAME);
  Destroy(idle_task_tid);
  Exit();
}
