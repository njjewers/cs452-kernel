#include <bwio.h>
#include <assert.h>
#include <util_tid_heap.h>

int main() {
  

  TidHeap heap;
  TidHeap_init(&heap);

  bwprintf(COM2, "init\r\n");
  // Check initialization correctness
  assert(TidHeap_peekPriority(&heap) == 0x7fffffff);

  bwprintf(COM2, "p1\r\n");
  TidHeap_push(&heap, 100, 6);
  bwprintf(COM2, "p1\r\n");
  TidHeap_push(&heap, 0, 1);
  bwprintf(COM2, "p1\r\n");
  TidHeap_push(&heap, 3, 3);
  bwprintf(COM2, "p1\r\n");
  TidHeap_push(&heap, 6, 5);
  bwprintf(COM2, "p1\r\n");
  TidHeap_push(&heap, 5, 4);
  bwprintf(COM2, "p1\r\n");
  TidHeap_push(&heap, 1, 2);

  bwprintf(COM2, "r1\r\n");
  assert(TidHeap_peekPriority(&heap) == 0);
  assert(TidHeap_popTid(&heap) == 1);

  bwprintf(COM2, "r1\r\n");
  assert(TidHeap_peekPriority(&heap) == 1);
  assert(TidHeap_popTid(&heap) == 2);

  bwprintf(COM2, "r1\r\n");
  assert(TidHeap_peekPriority(&heap) == 3);
  assert(TidHeap_popTid(&heap) == 3);

  bwprintf(COM2, "r1\r\n");
  assert(TidHeap_peekPriority(&heap) == 5);
  assert(TidHeap_popTid(&heap) == 4);

  bwprintf(COM2, "r1\r\n");
  assert(TidHeap_peekPriority(&heap) == 6);
  assert(TidHeap_popTid(&heap) == 5);

  bwprintf(COM2, "r1\r\n");
  assert(TidHeap_peekPriority(&heap) == 100);
  assert(TidHeap_popTid(&heap) == 6);

  bwprintf(COM2, "e1\r\n");
  // Queue should be empty...
  assert(TidHeap_peekPriority(&heap) == 0x7fffffff);

  return 0;
}
