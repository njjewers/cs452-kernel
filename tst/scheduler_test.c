#include <k_globals.h>
#include <k_scheduler.h>
#include <bwio.h>
#include <assert.h>

void setupTasks(KGlobals *globals);
void testScheduler_addTasks(KGlobals *globals);
void testScheduler_returnsHighestPriorityTasks(KGlobals *globals);
void testScheduler_removeTasks(KGlobals *globals);

int main() {
  KGlobals globals;
  globals.ready_mask = 0;

  TaskDescriptor a, b, c, d, e, f, g;
  a.tid = 1;
  b.tid = 3; c.tid = 4; d.tid = 5; e.tid = 6;
  f.tid = 7; g.tid = 8;
  
  a.priority = 2;
  b.priority = c.priority = d.priority = e.priority = 1;
  f.priority = g.priority = 3;

  add_ready_task(&globals, &a);
  add_ready_task(&globals, &b);
  add_ready_task(&globals, &c);
  add_ready_task(&globals, &d);
  add_ready_task(&globals, &e);
  add_ready_task(&globals, &f);
  add_ready_task(&globals, &g);

  testScheduler_addTasks(&globals);
  testScheduler_returnsHighestPriorityTasks(&globals);
  testScheduler_removeTasks(&globals);
  
  return 0;
}

void testScheduler_addTasks(KGlobals *globals) {
  TaskDescriptor* task;
  int priority;

  // 1. Queues with priorities 3, 2, 1 are not empty: 0b1110
  assert(globals->ready_mask == 14);

  // 3. Assert queue with priority 1 has 4 elements
  priority = 1;
  assert(globals->taskQueue[priority].head->tid == 3);
  assert(globals->taskQueue[priority].tail->tid == 6);

  task = globals->taskQueue[priority].head;
  assert(task->prev_ready == 0);
  assert(task->next_ready->tid == 4);
  
  task = task->next_ready;
  assert(task->tid == 4);
  assert(task->prev_ready->tid == 3);
  assert(task->next_ready->tid == 5);

  task = task->next_ready;
  assert(task->tid == 5);
  assert(task->prev_ready->tid == 4);
  assert(task->next_ready->tid == 6);

  task = task->next_ready;
  assert(task->tid == 6);
  assert(task->prev_ready->tid == 5);
  assert(task->next_ready == 0);

  // 4. Assert queue with priority 2 has only 1 element
  priority = 2;
  assert(globals->taskQueue[priority].head->tid == 1);
  assert(globals->taskQueue[priority].tail->tid == 1);
  assert(globals->taskQueue[priority].head->next_ready == 0);
  assert(globals->taskQueue[priority].head->prev_ready == 0);

  // 5. Assert queue with priority 3 has 2 elements
  priority = 3;
  assert(globals->taskQueue[priority].head->tid == 7);
  assert(globals->taskQueue[priority].tail->tid == 8);

  task = globals->taskQueue[priority].head;
  assert(task->prev_ready == 0);
  assert(task->next_ready->tid == 8);

  task = task->next_ready;
  assert(task->tid == 8);
  assert(task->prev_ready->tid == 7);
  assert(task->next_ready == 0);
}

void testScheduler_returnsHighestPriorityTasks(KGlobals *globals) {
  TaskDescriptor* task;

  // 1. Returns highest priority tasks (priority=3)
  task = get_next_ready_task(globals);
  assert(task->tid == 7);
  task = get_next_ready_task(globals);
  assert(task->tid == 8);

  // 2. Rotates them in the queue
  task = get_next_ready_task(globals);
  assert(task->tid == 7);
  task = get_next_ready_task(globals);
  assert(task->tid == 8);
}

void testScheduler_removeTasks(KGlobals *globals) {
  TaskDescriptor* task;

  // 1. Queues with priorities 3, 2, 1 are not empty: 0b1110
  assert(globals->ready_mask == 14);

  // 2. Returns highest priority tasks (priority=3)
  task = get_next_ready_task(globals);
  assert(task->tid == 7);
  remove_ready_task(globals, task);

  // 3. When tid=7 is removed, tid=8 is returned all the time
  task = get_next_ready_task(globals);
  assert(task->tid == 8);
  task = get_next_ready_task(globals);
  assert(task->tid == 8);
  remove_ready_task(globals, task);

  // 4. Queues with priority 2, 1 are non empty, 3 is empty: 0b0110
  assert(globals->ready_mask == 6);

  // 5. Returns highest priority tasks (priority=2)
  task = get_next_ready_task(globals);
  assert(task->tid == 1);
  task = get_next_ready_task(globals);
  assert(task->tid == 1);
  remove_ready_task(globals, task);

  // 6. Queue with priority 1 isn't empty, 2, 3 are empty: 0b0010
  assert(globals->ready_mask == 2);

  // 5. Returns highest priority tasks (priority=2)
  assert(get_next_ready_task(globals)->tid == 3);
  assert(get_next_ready_task(globals)->tid == 4);
  task = get_next_ready_task(globals);
  assert(task->tid == 5);
  assert(get_next_ready_task(globals)->tid == 6);
  assert(get_next_ready_task(globals)->tid == 3);

  remove_ready_task(globals, task);
  assert(get_next_ready_task(globals)->tid == 4);
  assert(get_next_ready_task(globals)->tid == 6);
  assert(get_next_ready_task(globals)->tid == 3);
  task = get_next_ready_task(globals);
  assert(task->tid == 4);

  remove_ready_task(globals, task);
  assert(get_next_ready_task(globals)->tid == 6);
  task = get_next_ready_task(globals);
  assert(task->tid == 3);

  remove_ready_task(globals, task);
  assert(get_next_ready_task(globals)->tid == 6);
  task = get_next_ready_task(globals);
  assert(task->tid == 6);

  remove_ready_task(globals, task);
  task = get_next_ready_task(globals);
  assert(task == 0);
}
