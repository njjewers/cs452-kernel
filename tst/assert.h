#ifndef __ASSERT_H__
#define __ASSERT_H__

#include "bwio.h"

# define assert(expr) \
  ((expr) ? 0 : __assert_fail (#expr, __FILE__, __LINE__))

void __assert_fail (const char *__assertion, const char *__file, unsigned int __line) {
  bwprintf(COM2, "Test failed in %s, line %u: %s\r\n", __file, __line, __assertion);
}

#endif
